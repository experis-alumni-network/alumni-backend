using Alumni_backend.Models.Domain;
using Alumni_backend.Service;
using Alumni_backend.Controllers;
using Alumni_backend.Profiles;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;
using AutoMapper;
using Alumni_backend.Models.DTOs.Posts;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using System.Net;
using Microsoft.AspNetCore.Mvc;

namespace alumni_backend_tests
{
    public class PostControllerTests
    {
        private Post GetPostByIdMockData(int id)
        {
            Post post = new Post()
            {
                PostId = 1,
                Title = "test title",
                Content = "hello, this is a post!",
                LastUpdated = DateTime.Now,
                CreationDate = DateTime.Now,
                AuthorUserId = 1,
                Type = PostType.VANILLA
            };
            return post;
        }

        [Fact]
        public async void GetPost_CheckIfExistingPostCanBeFetched_ShouldReturn1()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.AddProfile<PostProfile>();
            });

            var mapper = config.CreateMapper();

            var mockService = new Mock<PostService>(null);
            mockService.Setup(service => service.GetByIdAsync(1))
                .ReturnsAsync(GetPostByIdMockData(1));
            var controller = new PostsController(mockService.Object, mapper);

            var result = await controller.GetPost(1);

            var objectResult = result.Value.PostId;

            int actual = objectResult;

            int expected = 1;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public async void GetPost_CheckIfUnexistingPostCannotBeFetched_ShouldReturnHttp404()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.AddProfile<PostProfile>();
            });

            var mapper = config.CreateMapper();

            var mockService = new Mock<PostService>(null);
            mockService.Setup(service => service.GetByIdAsync(1))
                .ReturnsAsync(GetPostByIdMockData(1));
            var controller = new PostsController(mockService.Object, mapper);

            //Get post with Id 69. It does not exist in the database
            var result = await controller.GetPost(69);

            var objectResult = result.Result as IStatusCodeActionResult;

            int actual = (int)objectResult.StatusCode;

            int expected = (int)HttpStatusCode.NotFound;

            Assert.Equal(expected, actual);
        }

    }
}
