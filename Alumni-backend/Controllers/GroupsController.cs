﻿using Alumni_backend.DAL;
using Alumni_backend.Models.Domain;
using Alumni_backend.Models.DTOs.Groups;
using Alumni_backend.Models.DTOs.Posts;
using Alumni_backend.Models.DTOs.User;
using Alumni_backend.Service;
using Alumni_backend.Service.Authentication;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Alumni_backend.Controllers
{
    /// <summary>
    /// Controller for Group related endpoints.
    /// </summary>
    [ApiController]
    [Route("api/v1/group")]
    public class GroupsController : ControllerBase
    {
        private readonly GroupService _service;
        private readonly IMapper _mapper;

        public GroupsController(GroupService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;

        }

        /// <summary>
        /// Endpoint for retrieving all Groups from the database.
        /// </summary>
        /// <returns>
        /// Returns a list of groups as a list of GroupReadDTO's
        /// </returns>
        [Authorize]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<GroupReadDTO>>> GetGroups()
        {
            return _mapper.Map<List<GroupReadDTO>>(await _service.GetAllAsync());

        }
        /// <summary>
        /// Endpoint for retrieving a specific Group from the database.
        /// </summary>
        /// <param name="id">
        /// Group ID
        /// </param>
        /// <returns>
        /// Returns a single Group as a GroupReadDTO
        /// </returns>
        [Authorize]
        [HttpGet("{id}")]
        public async Task<ActionResult<GroupReadDTO>> GetGroup(int id)
        {
            Group group = await _service.GetByIdAsync(id);

            if (group == null)
            {
                return NotFound();
            }

            return _mapper.Map<GroupReadDTO>(group);
        }

        /// <summary>
        /// Endpoint for updating a group. 
        /// </summary>
        /// <param name="id">
        /// Group ID
        /// </param>
        /// <param name="dtoGroup">
        /// GroupEditDto, provided by the front end as json. 
        /// </param>
        /// <returns>
        /// Returns the updated
        /// </returns>
        [Authorize]
        [HttpPut("update/{id}")]
        public async Task<IActionResult> PutGroup(int id, GroupEditDTO dtoGroup)
        {

            if (!_service.EntityExists(id))
            {
                return NotFound();
            }

            Group controlGroup = await _service.GetByIdAsync(id);
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            User loggedInUser = await AuthService.GetLoggedInUser(identity, _service._context);

            if (controlGroup.AdminId != loggedInUser.UserId)
            {
                return BadRequest("You are not the admin of this group");
            }

            
            await _service.UpdateGroupAsync(id, dtoGroup);

            Group domainGroup = await _service.GetByIdAsync(id);

            return CreatedAtAction("GetGroup",
                new { id = domainGroup.GroupId },
                _mapper.Map<GroupReadDTO>(domainGroup));
        }

        /// <summary>
        /// Endpoint for adding a Group to the database. 
        /// Adds the currently logged in user as admin and a member
        /// </summary>
        /// <param name="dtoGroup">
        /// GroupCreateDTO, provided by front end as json
        /// </param>
        /// <returns>
        /// Returns the created group as a GroupReadDTO
        /// </returns>
        [Authorize]
        [HttpPost("create")]
        public async Task<ActionResult<Group>> PostGroup(GroupCreateDTO dtoGroup)
        {

            var identity = HttpContext.User.Identity as ClaimsIdentity;
            User loggedInUser = await AuthService.GetLoggedInUser(identity, _service._context);
            
            Group domainGroup = _mapper.Map<Group>(dtoGroup);
            domainGroup.AdminId = loggedInUser.UserId;
            List<User> temp = new();
            temp.Add(loggedInUser);
            domainGroup.Members = temp;

            await _service.AddAsync(domainGroup);

            return CreatedAtAction("GetGroup",
                new { id = domainGroup.GroupId },
                _mapper.Map<GroupReadDTO>(domainGroup));
        }


        /// <summary>
        /// Endpoint for retrieving a list of Groups where a specific user is a member
        /// </summary>
        /// <param name="userId">
        /// User ID
        /// </param>
        /// <returns>
        /// Returns a list of Groups as a list of GroupReadDTO
        /// </returns>
        [Authorize]
        [HttpGet("user/{userId}")]
        public async Task<ActionResult<IEnumerable<GroupReadDTO>>> GetGroupsForUser(int userId)
        {
            return _mapper.Map<List<GroupReadDTO>>(await _service.GetAllGroupsForUser(userId));
        }
         

        /// <summary>
        /// Endpoint for retrieving all posts for a group
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns>
        /// Returns a list of posts as PostReadDTOs
        /// </returns>
        [Authorize]
        [HttpGet("{groupId}/posts")]
        public async Task<ActionResult<IEnumerable<PostReadDTO>>> GetPostsForGroup(int groupId)
        {
            //TODO edit. This checks for groups with the user id.
            if (!_service.EntityExists(groupId))
            {
                return NotFound();
            }

            return _mapper.Map<List<PostReadDTO>>(await _service.GetPostsForGroup(groupId));
        }

        /// <summary>
        /// Endpoint for retrieving members from a group
        /// </summary>
        /// <param name="groupId">
        /// Group Id
        /// </param>
        /// <returns>
        /// Returns a list of users as UserReadDTO
        /// </returns>
        [Authorize]
        [HttpGet("{groupId}/members")]
        public async Task<ActionResult<IEnumerable<UserReadDTO>>> GetGroupMembers(int groupId)
        {
            if (!_service.EntityExists(groupId))
            {
                return NotFound();
            }

            //List<User> u = await _service.GetUsersInGroup(groupId);
            return _mapper.Map<List<UserReadDTO>>(await _service.GetUsersInGroup(groupId));
        }

        /// <summary>
        /// Endpoint for joining a group. 
        /// </summary>
        /// <param name="groupId">
        /// Group Id
        /// </param>
        /// <returns>
        /// Returns the Group as a GroupReadDTO
        /// </returns>
        [Authorize]
        [HttpPut("join/{groupId}")]
        public async Task<ActionResult<GroupReadDTO>> JoinGroup(int groupId)
        {
            Group groupToJoin = await _service.GetByIdAsync(groupId);
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            User loggedInUser = await AuthService.GetLoggedInUser(identity, _service._context);

            if (groupToJoin.Members.Contains(loggedInUser))
            {
                return BadRequest("You are already a member");
            }
            else
            {
                await _service.JoinGroup(groupId, loggedInUser);
            }

            return _mapper.Map<GroupReadDTO>(await _service.GetByIdAsync(groupId));
        }

        /// <summary>
        /// Endpoint for adding one or more users to a group. Most used to invite users to a private group. 
        /// </summary>
        /// <param name="groupId">
        /// Group Id</param>
        /// <param name="userIds">
        /// List of userIds, provided by the frontend as json
        /// </param>
        /// <returns>The group</returns>
        [Authorize]
        [HttpPut("invite/{groupId}")]
        public async Task<ActionResult<GroupReadDTO>> InviteUsersToGroup(int groupId, List<int> userIds)
        {
            Group groupToJoin = await _service.GetByIdAsync(groupId);           
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            User loggedInUser = await AuthService.GetLoggedInUser(identity, _service._context);

           
           if (groupToJoin.Members.Contains(loggedInUser))
           {
                await _service.InviteUsersToGroup(groupId, userIds);
           }
           else
           {
                return BadRequest("You are not a member of this group!");
           }

            return _mapper.Map<GroupReadDTO>(await _service.GetByIdAsync(groupId));
        }   
    }
}
