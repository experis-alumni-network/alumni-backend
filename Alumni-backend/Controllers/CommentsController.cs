﻿using Alumni_backend.DAL;
using Alumni_backend.Models.Domain;
using Alumni_backend.Models.DTOs.Comments;
using Alumni_backend.Service;
using Alumni_backend.Service.Authentication;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
namespace Alumni_backend.Controllers
{
    /// <summary>
    /// Controller for comment releated endpoints. 
    /// </summary>
    [ApiController]
    [Route("api/v1/comment")]
    public class CommentsController : ControllerBase
    {
        private readonly CommentService _service;
        private readonly IMapper _mapper;

        public CommentsController(CommentService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;

        }

        /// <summary>
        /// Endpoint for retrieving all Comments
        /// </summary>
        /// <returns>
        /// Returns a list of Comments as CommentEditReadDTO'S
        /// </returns>
        [Authorize]
        [HttpGet()]
        public async Task<ActionResult<IEnumerable<CommentEditReadDTO>>> GetComments()
        {
            return _mapper.Map<List<CommentEditReadDTO>>(await _service.GetAllAsync());
        }

        /// <summary>
        /// Endpoint for retrieving a single comment
        /// </summary>
        /// <param name="id">
        /// Comment Id
        /// </param>
        /// <returns>
        /// Returns a Comment as a CommentEditReadDTO
        /// </returns>
        [Authorize]
        [HttpGet("{id}")]
        public async Task<ActionResult<CommentEditReadDTO>> GetComment(int id)
        {
            Comment comment = await _service.GetByIdAsync(id);

            if (comment == null)
            {
                return NotFound();
            }

            return _mapper.Map<CommentEditReadDTO>(comment);
        }

        /// <summary>
        /// Endpoint for updating the content of a comment.
        /// </summary>
        /// <param name="id">
        /// Comment Id
        /// </param>
        /// <param name="content">
        /// String content of a comment, provided by frontend as json
        /// </param>
        /// <returns>
        /// Returns the updated comment as a CommentEditReadDTO
        /// </returns>
        [Authorize]
        [HttpPut("{id}")]
        public async Task<ActionResult<CommentEditReadDTO>> ChangeCommentContent(int id, string content)
        {
            if (!_service.EntityExists(id))
            {
                return NotFound();
            }
            Comment controlComment = await _service.GetByIdAsync(id);

            var identity = HttpContext.User.Identity as ClaimsIdentity;
            User loggedInUser = await AuthService.GetLoggedInUser(identity, _service._context);

            if (controlComment.AuthorId != loggedInUser.UserId)
            {
                return BadRequest("You are not the author of this comment");
            }

            try
            {
                await _service.EditCommentAsync(id, content);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Comment does not exist");
            }

            Comment comment = await _service.GetByIdAsync(id);

            return _mapper.Map<CommentEditReadDTO>(comment);
        }

        /// <summary>
        /// Endpoint for adding a comment to the database. Used when a user comments on a post
        /// </summary>
        /// <param name="dtoComment">
        /// Comment as a CommentCreateDTO, provided by frontend as json
        /// </param>
        /// <returns>
        /// Returns a comment as a CommentEditReadDTO
        /// </returns>
        [Authorize]
        [HttpPost()]
        public async Task<ActionResult<Comment>> PostComment(CommentCreateDTO dtoComment)
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            User loggedInUser = await AuthService.GetLoggedInUser(identity, _service._context);

            Comment domainComment = _mapper.Map<Comment>(dtoComment);
            domainComment.AuthorId = loggedInUser.UserId;
            await _service.AddAsync(domainComment);

            return CreatedAtAction("GetComment",
                new { id = domainComment.CommentId },
                _mapper.Map<CommentEditReadDTO>(domainComment));
        }

    }
}
