﻿using Alumni_backend.Models.Domain;
using Alumni_backend.Models.DTOs.Posts;
using Alumni_backend.Models.DTOs.Topics;
using Alumni_backend.Service;
using Alumni_backend.Service.Authentication;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Alumni_backend.Controllers
{
    /// <summary>
    /// Controller for Topic related endpoints.
    /// </summary>
    [ApiController]
    [Route("api/v1/topic")]
    public class TopicsController : ControllerBase
    {
        private readonly TopicService _service;
        private readonly IMapper _mapper;

        public TopicsController(TopicService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        /// <summary>
        /// Endpoint for retrieving all Topics from the database.
        /// </summary>
        /// <returns>
        /// List of Topic as a list of TopicEditReadDTO'S
        /// </returns>

        [Authorize]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TopicEditReadDTO>>> GetTopics()
        {
            return _mapper.Map<List<TopicEditReadDTO>>(await _service.GetAllAsync());
        }

        /// <summary>
        /// Endpoint for retrieving a specific Topic from the database.
        /// </summary>
        /// <param name="id"> 
        /// Topic ID 
        /// </param>
        /// <returns>
        /// Returns a single Topic as a TopicEditReadDTO
        /// </returns>
        [Authorize]
        [HttpGet("{id}")]
        public async Task<ActionResult<TopicEditReadDTO>> GetTopic(int id)
        {
            Topic topic = await _service.GetByIdAsync(id);

            if (topic == null)
            {
                return NotFound();
            }

            return _mapper.Map<TopicEditReadDTO>(topic);
        }

        /// <summary>
        /// Endpoint for adding a Topic to the database
        /// </summary>
        /// <param name="dtoTopic"> 
        /// TopicCreateDTO, wil be used by _mapper to create a Topic object 
        /// </param>
        /// <returns> 
        /// Returns the created Topic as a TopicEditeReadDTO 
        /// </returns>
        //[Authorize]
        [HttpPost]
        public async Task<ActionResult<Topic>> PostTopic(TopicCreateDTO dtoTopic)
        {
            Topic domainTopic = _mapper.Map<Topic>(dtoTopic);

            await _service.AddAsync(domainTopic);

            return CreatedAtAction("GetTopic",
                new { id = domainTopic.TopicId },
                _mapper.Map<TopicEditReadDTO>(domainTopic));
        }


        /// <summary>
        /// Endpoint for retrieving posts for a specific topic.
        /// Filters out posts that are in private groups that the users is not a member of. 
        /// </summary>
        /// <param name="id">
        /// Topic ID
        /// </param>
        /// <returns>
        /// Returns a list of Posts as a list of PostReadDTO's
        /// </returns>
        [Authorize]
        [HttpGet("{id}/post")]
        public async Task<ActionResult<IEnumerable<PostReadDTO>>> GetPostsForTopic(int id)
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            User loggedInUser = await AuthService.GetLoggedInUser(identity, _service._context);

            return _mapper.Map<List<PostReadDTO>>(await _service.GetAllPostFromTopic(loggedInUser, id));

        }


        /// <summary>
        /// Endpoint for subscribing to a topic. 
        /// Adds the user to the list of interested users
        /// </summary>
        /// <param name="id">
        /// Topic ID
        /// </param>
        /// <returns>
        /// Returns TopicEditReadDto
        /// </returns>
        [Authorize]
        [HttpPut("subscribe/{id}")]
        public async Task<ActionResult<TopicEditReadDTO>> SubscribeToTopic(int id)
        {
            if (!_service.EntityExists(id))
            {
                return BadRequest("Cant find topic!");
            }

            Topic topicToSubTo = await _service.GetByIdAsync(id);
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            User loggedInUser = await AuthService.GetLoggedInUser(identity, _service._context);

            if (topicToSubTo.InterestedUsers.Contains(loggedInUser))
            {
                return BadRequest("You have allready subed to this topic!!");
            }
            else
            {
                return _mapper.Map<TopicEditReadDTO>(await _service.SubscribeToTopic(loggedInUser, id));
            }
        }       
    }
}
