﻿using Alumni_backend.DAL;
using Alumni_backend.Models.Domain;
using Alumni_backend.Models.DTOs.Posts;
using Alumni_backend.Service;
using Alumni_backend.Service.Authentication;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;


namespace Alumni_backend.Controllers
{

    /// <summary>
    /// Controller for all Post related endpoints
    /// </summary>
    [ApiController]
    [Route("api/v1/post")]
    public class PostsController : ControllerBase
    {
        private readonly PostService _service;
        private readonly IMapper _mapper;

        public PostsController(PostService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;

        }

        /// <summary>
        /// Endpoint for retrieving all posts viewable for the currently logged in user from the database
        /// </summary>
        /// <returns>
        /// Returns a list of posts as PostReadDTO'S
        /// </returns>
        //[Authorize]
        [HttpGet()]
        public async Task<ActionResult<IEnumerable<PostReadDTO>>> GetPosts()
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            User loggedInUser = await AuthService.GetLoggedInUser(identity, _service._context);

            return _mapper.Map<List<PostReadDTO>>(await _service.GetAllPostsAsync(loggedInUser));
        }

        /// <summary>
        /// Endpoint for retrieving all posts posted by a user
        /// </summary>
        /// <param name="userId">
        /// userId
        /// </param>
        /// <returns>
        /// Returns a list of posts as PostReadDTO'S
        /// </returns>
        [Authorize]
        [HttpGet("postedBy/{userId}")]
        public async Task<ActionResult<IEnumerable<PostReadDTO>>> GetAllPostedByUser(int userId)
        {

            return _mapper.Map<List<PostReadDTO>>(await _service.GetAllPostedByUser(userId));
        }

        /// <summary>
        /// Endpoint for retrieving all posts relevant for a user
        /// </summary>
        /// <param name="userId">
        /// UserId
        /// </param>
        /// <returns>
        /// List of posts as PostReadDTO
        /// </returns>
        //[Authorize]
        [HttpGet("getRelevantPosts/user/{userId}")]
        public async Task<ActionResult<IEnumerable<PostReadDTO>>> GetRelevantPosts(int userId)
        {

            return _mapper.Map<List<PostReadDTO>>(await _service.GetAllRelevantPosts(userId));

        }

        /// <summary>
        /// Endpoint for retrieving a single posts from the database
        /// </summary>
        /// <param name="id">
        /// Post Id
        /// </param>
        /// <returns>
        /// Returns a single post as a PostReadDTO
        /// </returns>
        [Authorize]
        [HttpGet("{id}")]
        public async Task<ActionResult<PostReadDTO>> GetPost(int id)
        {
            Post post = null;
            try
            {
                post = await _service.GetByIdAsync(id);
            }
            catch
            {
                
            }

            if (post == null)
            {
                return NotFound();
            }

            return _mapper.Map<PostReadDTO>(post);
        }

        [SwaggerOperation(Summary = "Type options: 0 = VANILLA, 1 = EVENT")]
        /// <summary>
        /// Endpoint for updating a post.
        /// Type options: 0 = VANILLA, 1 = EVENT
        /// </summary>
        /// <param name="id">
        /// Post Id
        /// </param>
        /// <param name="postDto">
        /// Post object as a PostEditDto, provided by frontend as json
        /// </param>
        /// <returns>
        /// The edited Post object as a PostReadDTO
        /// </returns>
        [Authorize]
        [HttpPut("update/{id}")]
        public async Task<IActionResult> PutPost(int id, PostEditDTO postDto)
        {

            if (!_service.EntityExists(id))
            {
                return BadRequest();
            }

            Post controlPost = await _service.GetByIdAsync(id);
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            User loggedInUser = await AuthService.GetLoggedInUser(identity, _service._context);

            if (controlPost.AuthorUserId != loggedInUser.UserId)
            {
                return BadRequest("You are not the author of this post");
            }


            try
            {
                await _service.UpdatePostAsync(id, postDto);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Something went wrong");
            }

            Post domainPost = await _service.GetByIdAsync(id);

                return CreatedAtAction("GetPost",
                new { id = domainPost.PostId },
                _mapper.Map<PostReadDTO>(domainPost));

        }

        [SwaggerOperation(Summary = "Type options: 0 = VANILLA, 1 = EVENT")]
        /// <summary>
        /// Endpoint for adding a post to the database. 
        /// If groupId = 0, the field should be removed from the json body.
        /// Type options: 0 = VANILLA, 1 = EVENT
        /// </summary>
        /// <param name="dtoPost">
        /// Post object as a postCreateDTO, provided by the frontend as json
        /// </param>
        /// <returns>
        /// Returns a post as a PostCreatDTO
        /// </returns>
        [Authorize]
        [HttpPost()]
        public async Task<ActionResult<Post>> PostPost(PostCreateDTO dtoPost) 
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            User loggedInUser = await AuthService.GetLoggedInUser(identity, _service._context);

     
            List<int> topics = dtoPost.Topics;

            GroupService groupService = new GroupService(_service._context);
            List<Group> groupList = (List<Group>)await groupService.GetAllGroupsForUser(loggedInUser.UserId);
            bool isMember = false;
            foreach (Group group in groupList)
            {
                if(group.GroupId == dtoPost.Group) 
                {
                    isMember = true;
                    break;
                }
            }
            if (!isMember && dtoPost.Group != null)
            {
                return BadRequest("You can't post to a group you are not a member of.");
            }

            Post domainPost = _mapper.Map<Post>(dtoPost);
            domainPost.LastUpdated = DateTime.Now;
            domainPost.CreationDate = domainPost.LastUpdated;
            domainPost.AuthorUserId = loggedInUser.UserId;

            await _service.AddPostAsync(domainPost, topics);

            return CreatedAtAction("GetPost",
                new { id = domainPost.PostId },
                _mapper.Map<PostReadDTO>(domainPost));
        }
    }

}
