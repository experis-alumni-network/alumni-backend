﻿using Alumni_backend.Models.Domain;
using Alumni_backend.Models.DTOs.Comments;
using Alumni_backend.Models.DTOs.PostEvents;
using Alumni_backend.Models.DTOs.User;
using Alumni_backend.Service;
using Alumni_backend.Service.Authentication;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Alumni_backend.Controllers
{
    /// <summary>
    /// Controller for event releated endpoints.
    /// </summary>
    [ApiController]
    [Route("api/v1/event")]
    public class PostEventController : ControllerBase
    {
        private readonly PostEventService _service;
        private readonly IMapper _mapper;

        public PostEventController(PostEventService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;

        }
        /// <summary>
        /// Helper function to manully map Post and Events to a flat body
        /// </summary>
        /// <param name="postEvent"></param>
        /// <returns></returns>
        private PostEventReadDTO readMapPostAndEventProps(PostEvent postEvent)
        {
            PostEventReadDTO dto = new PostEventReadDTO();
            dto.PostEventId = postEvent.PostEventId;
            dto.AllowedGuests = postEvent.AllowedGuests;
            dto.StartTime = postEvent.StartTime;
            dto.EndTime = postEvent.EndTime;
            dto.BannerImg = postEvent.BannerImg;
            dto.IsPrivate = postEvent.IsPrivate;
            dto.RelatedPost = postEvent.RelatedPostId;
            dto.Title = postEvent.RelatedPost.Title;
            dto.Content = postEvent.RelatedPost.Content;
            dto.LastUpdated = postEvent.RelatedPost.LastUpdated;
            dto.CreationDate = postEvent.RelatedPost.CreationDate;
            dto.Author = postEvent.RelatedPost.AuthorUserId;
            dto.Type = postEvent.RelatedPost.Type;

            dto.Group = 0;
            try
            {
                dto.Group = (int)postEvent.RelatedPost.GroupId;
            }
            catch(InvalidOperationException)
            {
                
            }
            dto.Comments = new List<CommentEditReadDTO>();
            dto.SubscribedUsers = new List<int>();



            
            if (postEvent.RelatedPost.Comments != null)
            {
                foreach (Comment comment in postEvent.RelatedPost.Comments)
                {
                    dto.Comments.Add(_mapper.Map<CommentEditReadDTO>(comment));
                }
            }

            if (postEvent.RelatedPost.SubscribedUsers != null)
            {
                foreach (User user in postEvent.RelatedPost.SubscribedUsers)
                {
                    dto.SubscribedUsers.Add(user.UserId);
                }
            }

            if (postEvent.RelatedPost.Topics != null) {
                dto.Topics = postEvent.RelatedPost.Topics.Select(b => b.TopicId).ToList();
            }


            return dto;
        }
        [SwaggerOperation(Summary = "Type options: 0 = VANILLA, 1 = EVENT")]
        /// <summary>
        /// Endpoint for retrieving all PostEvents
        /// </summary>
        /// <returns>
        /// Return a list of PostEvents as PostEventReadDTO'S
        /// </returns>
        [Authorize]
        [HttpGet()]
        public async Task<ActionResult<IEnumerable<PostEventReadDTO>>> GetPostEvents()
        {
            List<PostEventReadDTO> eventList = new List<PostEventReadDTO>();
            foreach (var postEvent in await _service.GetAllAsync())
            {
                eventList.Add(readMapPostAndEventProps(postEvent));
            }
            return eventList;
        }

        /// <summary>
        /// Endpoint for retrieving all users that has accepted the invite to the event. 
        /// </summary>
        /// <param name="eventId">
        /// PostEvent Id
        /// </param>
        /// <returns>
        /// Returns a list of users as UserReadDTO's
        /// </returns>
        [Authorize]
        [HttpGet("{eventId}/acceptedusers")]
        public async Task<ActionResult<IEnumerable<UserReadDTO>>> GetAcceptedGuestsForEvent(int eventId)
        {
            return _mapper.Map<List<UserReadDTO>>(await _service.GetAllAcceptedGuestsForEventAsync(eventId));
        }

        /// <summary>
        /// Endpoint for retrieving all users that has declined the invite to the event. 
        /// </summary>
        /// <param name="eventId">
        /// PostEvent Id
        /// </param>
        /// <returns>
        /// Returns a list of users as UserReadDTO's
        /// </returns>
        [Authorize]
        [HttpGet("{eventId}/declinedusers")]
        public async Task<ActionResult<IEnumerable<UserReadDTO>>> GetDeclinedGuestsForEvent(int eventId)
        {
            return _mapper.Map<List<UserReadDTO>>(await _service.GetAllDeclinedUsersForEventAsync(eventId));
        }


        /// <summary>
        /// Endpoint for retrieving all users that has answered maby to the invite.
        /// </summary>
        /// <param name="eventId">
        /// PostEvent Id
        /// </param>
        /// <returns>
        /// Returns a list of users as UserReadDTO's
        /// </returns>
        [Authorize]
        [HttpGet("{eventId}/maybeusers")]
        public async Task<ActionResult<IEnumerable<UserReadDTO>>> GetMaybeGuestsForEvent(int eventId)
        {
            return _mapper.Map<List<UserReadDTO>>(await _service.GetAllMaybeUsersForEventAsync(eventId));
        }


        /// <summary>
        /// Endpoint for retrieving all users that has not answerd the invite.
        /// </summary>
        /// <param name="eventId">
        /// PostEvent Id
        /// </param>
        /// <returns>
        /// Returns a list of users as UserReadDTO's
        /// </returns>
        [Authorize]
        [HttpGet("{eventId}/unansweredusers")]
        public async Task<ActionResult<IEnumerable<UserReadDTO>>> GetUnansweredGuestsForEvent(int eventId)
        {
            return _mapper.Map<List<UserReadDTO>>(await _service.GetAllUnansweredUsersForEventAsync(eventId));
        }

        /// <summary>
        /// Endpoint for retrieving all users that was invited.
        /// </summary>
        /// <param name="eventId">
        /// PostEvent Id
        /// </param>
        /// <returns>
        /// Returns a list of users as UserReadDTO's
        /// </returns>
        [Authorize]
        [HttpGet("{eventId}/invitedusers")]
        public async Task<ActionResult<IEnumerable<UserReadDTO>>> GetInvitedGuestsForEvent(int eventId)
        {
            return _mapper.Map<List<UserReadDTO>>(await _service.GetInvitedGuestsForEventAsync(eventId));
        }

        [SwaggerOperation(Summary = "Type options: 0 = VANILLA, 1 = EVENT")]
        /// <summary>
        /// Endpoint for retrieving
        /// set months = 0 for no filter
        /// </summary>
        /// <param name="id">
        /// User Id
        /// </param>
        /// <param name="months"></param>
        /// <returns>
        /// Returns a list of EventDto's
        /// </returns>
        [Authorize]
        [HttpGet("user/{userId}")]
        public async Task<ActionResult<IEnumerable<PostEventReadDTO>>> GetUpcommingEventsForUser(int userId, int months)
        {
            List<PostEventReadDTO> eventDtos = new List<PostEventReadDTO>();

            List<PostEvent> events = (List<PostEvent>)await _service.GetUpcommingEventsForUserAsync(userId, months);

            events.ForEach(e => eventDtos.Add(readMapPostAndEventProps(e)));


            return eventDtos;
        }

        [SwaggerOperation(Summary = "Type options: 0 = VANILLA, 1 = EVENT")]
        /// <summary>
        /// Endpoint retrieving a single PostEvent
        /// </summary>
        /// <param name="id">
        /// PostEvent Id
        /// </param>
        /// <returns>
        /// Returns a PostEventDTO
        /// </returns>
        [Authorize]
        [HttpGet("{eventId}")]
        public async Task<ActionResult<PostEventReadDTO>> GetPostEvent(int eventId)
        {
            PostEvent postEvent = await _service.GetByPostIdAsync(eventId);

            PostEventReadDTO dto = readMapPostAndEventProps(postEvent);
            if (postEvent == null)
            {
                return NotFound();
            }

            return dto;
        }

        /// <summary>
        /// Endpoint for retrieving a Short version of a PostEvent.
        /// </summary>
        /// <param name="postId">
        /// Post Id
        /// </param>
        /// <returns>
        /// Returns a PostEvenShortReadDTO
        /// </returns>
        [Authorize]
        [HttpGet("short/{postId}")]
        public async Task<ActionResult<PostEventShortReadDTO>> GetShortPostEvent(int postId)
        {
            PostEvent postEvent = await _service.GetByPostIdAsync(postId);

            if (postEvent == null)
            {
                return NotFound();
            }

            return _mapper.Map<PostEventShortReadDTO>(postEvent);
        }


        /// <summary>
        /// Endpoint for updating a PostEvent
        /// </summary>
        /// <param name="postId">
        /// Post id
        /// </param>
        /// <param name="dtoEditEvent">
        /// PostEventEdit dto.
        /// </param>
        /// <returns>
        /// Returns a PostEventReadDTO
        /// </returns>
        [Authorize]
        [HttpPut("{postId}")]
        public async Task<ActionResult<PostEventReadDTO>> PutPostByPostIdEvent(int postId, PostEventEditDTO dtoEditEvent)
        {

            PostEvent postEvent = await _service.GetByPostIdAsync(postId);

            if (!_service.EntityExists(postEvent.PostEventId))
            {
                return NotFound();
            }


            Post post = postEvent.RelatedPost;

            var identity = HttpContext.User.Identity as ClaimsIdentity;
            User loggedInUser = await AuthService.GetLoggedInUser(identity, _service._context);

            if (post.AuthorUserId != loggedInUser.UserId)
            {
                return BadRequest("You are not the author of this event");
            }



            post.Content = dtoEditEvent.Content;
            post.Title = dtoEditEvent.Title;
            post.Topics = await TopicIntListConverter.IntListToTopics(dtoEditEvent.Topics, _service._context);

            postEvent.AllowedGuests = dtoEditEvent.AllowedGuests;
            postEvent.StartTime = dtoEditEvent.StartTime;
            postEvent.EndTime = dtoEditEvent.EndTime;
            postEvent.BannerImg = dtoEditEvent.BannerImg;
            postEvent.IsPrivate = dtoEditEvent.IsPrivate;
            await _service.UpdateAsync(postEvent, post);

            return readMapPostAndEventProps(postEvent);

        }

        /// <summary>
        /// Endpoint for adding a PostEvent to the database
        /// </summary>
        /// <param name="dtoPostEvent">
        /// PostEvent object as a PostEventCreateDTO
        /// </param>
        /// <returns>
        /// Returns the PostEvent as PostEvenReadDTO
        /// </returns>
        [Authorize]
        [HttpPost()]
        public async Task<ActionResult<PostEventReadDTO>> PostPostEvent(PostEventCreateDTO dtoPostEvent)
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            User loggedInUser = await AuthService.GetLoggedInUser(identity, _service._context);


            GroupService groupService = new GroupService(_service._context);
            List<Group> groupList = (List<Group>)await groupService.GetAllGroupsForUser(loggedInUser.UserId);
            bool isMember = false;
            foreach (Group group in groupList)
            {
                if (group.GroupId == dtoPostEvent.Group)
                {
                    isMember = true;
                    break;
                }
            }
            if (!isMember && dtoPostEvent.Group != null)
            {
                return BadRequest("You can't post to a group you are not a member of.");
            }
            Post post = new Post();
            post.Title = dtoPostEvent.Title;
            post.Content = dtoPostEvent.Content;
            post.AuthorUserId = loggedInUser.UserId;
            post.CreationDate = DateTime.Now;
            post.LastUpdated = post.CreationDate;
            post.Type = PostType.EVENT;
            post.GroupId = dtoPostEvent.Group;

            post.Topics = new List<Topic>();
            try
            {
                post.Topics = await TopicIntListConverter.IntListToTopics(dtoPostEvent.Topics, _service._context);
            }
            catch
            {

            }



            PostEvent postEvent = new PostEvent();
            postEvent.AllowedGuests = dtoPostEvent.AllowedGuests;
            postEvent.StartTime = dtoPostEvent.StartTime;
            postEvent.EndTime = dtoPostEvent.EndTime;
            postEvent.BannerImg = dtoPostEvent.BannerImg;
            postEvent.IsPrivate = dtoPostEvent.IsPrivate;

            postEvent.RelatedPost = post;

            await _service.AddAsync(postEvent);

            return readMapPostAndEventProps(postEvent);
        }

    }


}
