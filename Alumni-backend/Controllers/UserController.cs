using Alumni_backend.DAL;
using Alumni_backend.Models.Domain;
using Alumni_backend.Models.DTOs.Users;
using Alumni_backend.Service;
using AutoMapper;
using Google.Apis.Auth;
using Microsoft.AspNetCore.Authentication.Google;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;
using System.Web.Http.Cors;
using System.Web;
using Microsoft.Extensions.Configuration;

using System.ComponentModel.DataAnnotations;
using Alumni_backend.Models.DTOs.User;
using System.Security.Claims;

using Alumni_backend.Service.Authentication;

namespace Alumni_backend.Controllers
{
    [ApiController]
    [Route("api/v1/user")]
    public class UserController : Controller
    {
        private readonly UserService _service;
        private readonly IMapper _mapper;
        private readonly JwtGenerator _jwtGenerator;

        public class AuthenticateRequest
        {
            [Required]
            public string IdToken { get; set; }
        }

        public UserController(UserService service, IMapper mapper, IConfiguration configuration) 
        {   
            _service = service;
            _mapper = mapper;
            _jwtGenerator = new JwtGenerator(configuration.GetValue<string>("JwtPrivateSigningKey"));

        }
        /// <summary>
        /// Endpoint for retrieving all Users from the database.
        /// </summary>
        /// <returns>
        /// List of Users as a list of TopicReadDTO's
        /// </returns>
        [Authorize]
        [HttpGet("getAll")]
        public async Task<ActionResult<IEnumerable<UserReadDTO>>> GetUsers()
        {
            return _mapper.Map<List<UserReadDTO>>(await _service.GetAllAsync());
        }


        /// <summary>
        /// Endpoint for retrieving a specific User by email from the database.
        /// </summary>
        /// <param name="email"> 
        /// Topic ID 
        /// </param>
        /// <returns>
        /// Returns a single User as a UserReadDTO
        /// </returns>
        [Authorize]
        [HttpGet("{email}")]
        public async Task<ActionResult<UserReadDTO>> GetUserByEmail(string email)
        {
            return _mapper.Map<UserReadDTO>(await _service.GetByEmailAsync(email));
        }

        /// <summary>
        /// Endpoint for retrieving a specific User by id from the database.
        /// </summary>
        /// <param name="id"> 
        /// Topic ID 
        /// </param>
        /// <returns>
        /// Returns a single User as a UserReadDTO
        /// </returns>

        [Authorize]
        [HttpGet("{id:int}")]
        public async Task<ActionResult<UserReadDTO>> GetUser(int id)
        {
            var user = await _service.GetByIdAsync(id);

            if(user == null)
            {
                return NotFound();
            }

            return _mapper.Map<UserReadDTO>(user);
        }

        /// <summary>
        /// Endpoint for retrieving a list of Users by ids from the database.
        /// </summary>
        /// <param name="id"> 
        /// Topic ID 
        /// </param>
        /// <returns>
        /// Returns a List of Users as UserReadDTO's
        /// </returns>

        [Authorize]
        [HttpGet("getMultipleUsers")]
        public async Task<ActionResult<IEnumerable<UserReadDTO>>> GetMultipleUsers([FromQuery] List<int> id)
        {
            var user = await _service.GetMultipleUsers(id);

            if (user == null)
            {
                return NotFound();
            }

            return _mapper.Map<List<UserReadDTO>>(user);
        }

        /// <summary>
        /// Endpoint for updating a User.
        /// </summary>
        /// <param name="id">
        /// Post Id
        /// </param>
        /// <param name="userDto">
        /// Post object as a UserEditDTO, provided by frontend as json
        /// </param>
        [Authorize]
        [HttpPatch("{id}")]
        public async Task<IActionResult> PutUser(int id, UserEditDTO userDto)
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            User loggedInUser = await AuthService.GetLoggedInUser(identity, _service._context);

            if (id != loggedInUser.UserId)
            {
                return BadRequest("You are not owner of this user!");
            }


            if (!_service.EntityExists(id))
            {
                return NotFound();
            }
            
            try
            {
                await _service.UpdateAsync(id, userDto);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("User does not exist");
            }



            return NoContent();
        }

        /// <summary>
        /// Endpoint for adding a User to the database
        /// </summary>
        /// <param name="user"> 
        /// UserCreateDTO, wil be used by _mapper to create a User object 
        /// </param>
        /// <returns> 
        /// Returns the created User as a UserReadDTO 
        /// </returns>
        [Authorize]
        [HttpPost("createUser")]
        public async Task<ActionResult<UserReadDTO>> PostUser([FromBody]UserCreateDTO user)
        {
            Console.WriteLine(user.Email);

            if (!_service.EntityExists(user.Email))
            {
                Console.WriteLine("Got here!");
                User domainUser = _mapper.Map<User>(user);
                await _service.AddAsync(domainUser);

                return CreatedAtAction("GetUser", new { id = domainUser.UserId },
                    _mapper.Map<UserCreateDTO>(domainUser));

            }
            return _mapper.Map<UserReadDTO>(await _service.GetByEmailAsync(user.Email));
        }

        /// <summary>
        /// Endpoint for authenticating a User
        /// </summary>
        /// <param name="data"> 
        /// Token data
        /// </param>
        /// <returns> 
        /// A HTTP response code 
        /// </returns>
        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody] AuthenticateRequest data)
        {
            GoogleJsonWebSignature.ValidationSettings settings = new GoogleJsonWebSignature.ValidationSettings();

            // Change this to your google client ID
            settings.Audience = new List<string>() { "834242937946-to54thu5pf9ssjm8p79eekotqv1nm71s.apps.googleusercontent.com" };

            GoogleJsonWebSignature.Payload payload = GoogleJsonWebSignature.ValidateAsync(data.IdToken, settings).Result;

            return Ok(new { AuthToken = _jwtGenerator.CreateUserAuthToken(payload.Email) });
        }

        /// <summary>
        /// Endpoint for getting the currently logged in User
        /// </summary>
        /// <returns> 
        /// A User object
        [Authorize]
        [HttpGet("getAuthorizedUser")]
        public async Task<ActionResult<UserReadDTO>> GetAuthenticated()
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            User loggedInUser = await AuthService.GetLoggedInUser(identity, _service._context);
            return  _mapper.Map<UserReadDTO>(loggedInUser);
            //await _service.UpdateAsync(domainUser);
        }
    }
}