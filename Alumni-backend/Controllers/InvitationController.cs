﻿using Alumni_backend.Models.Domain;
using Alumni_backend.Models.DTOs.Invitations;
using Alumni_backend.Service;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Alumni_backend.Controllers
{

    /// <summary>
    /// Controller for Invitation releated enpoints. 
    /// Thees endpoints are used to invite a user to an event.
    /// </summary>
    [ApiController]
    [Route("api/v1/invitation")]
    public class InvitationController : ControllerBase
    {
        private readonly InvitationService _service;
        private readonly IMapper _mapper;

        public InvitationController(InvitationService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;

        }

        [SwaggerOperation(Summary = "Answers: 0 = UNANSWERED, 1 = YES, 2 = NO, 3 = MAYBE")]
        /// <summary>
        /// Endpoint for retrieving all invitations
        /// Answers: 0 = UNANSWERED, 1 = YES, 2 = NO, 3 = MAYBE
        /// </summary>
        /// <returns>
        /// List of all invitations as InvitationEditReadDTO
        /// </returns>
        [Authorize]
        [HttpGet()]
        public async Task<ActionResult<IEnumerable<InvitationEditReadDTO>>> GetInvitations()
        {
            return _mapper.Map<List<InvitationEditReadDTO>>(await _service.GetAllAsync());
        }

        [SwaggerOperation(Summary = "Answers: 0 = UNANSWERED, 1 = YES, 2 = NO, 3 = MAYBE")]
        /// <summary>
        /// Endpoint for retrieving a single invitation
        /// Answers: 0 = UNANSWERED, 1 = YES, 2 = NO, 3 = MAYBE
        /// </summary>
        /// <param name="invitationId">
        /// Invitation Id
        /// </param>
        /// <returns>
        /// Return a Invitation as a InvitationEditReadDTO
        /// </returns>
        [Authorize]
        [HttpGet("{invitationId}")]
        public async Task<ActionResult<InvitationEditReadDTO>> GetInvitationByInvitationId(int invitationId)
        {
            Invitation invitation = await _service.GetByIdAsync(invitationId);

            if (invitation == null)
            {
                return NotFound();
            }

            return _mapper.Map<InvitationEditReadDTO>(invitation);
        }

        [SwaggerOperation(Summary = "Answers: 0 = UNANSWERED, 1 = YES, 2 = NO, 3 = MAYBE")]
        /// <summary>
        /// Endpoint for retrieving Invitation by Event Id and user Id
        /// Answers: 0 = UNANSWERED, 1 = YES, 2 = NO, 3 = MAYBE
        /// </summary>
        /// <param name="eventId">
        /// Event Id
        /// </param>
        /// <param name="userId">
        /// User Id
        /// </param>
        /// <returns>
        /// Returns a Invitation as a InvitationEditReadDTO
        /// </returns>
        [Authorize]
        [HttpGet("event/{eventId}/user/{userId}")]
        public async Task<ActionResult<InvitationEditReadDTO>> GetInvitationByEventIdAndUserId(int eventId, int userId)
        {
            Invitation invitation = await _service.GetInvitationByEventIdAndUserId(eventId, userId);

            if (invitation.InvitationId == 0)
            {
                return NotFound("Invite does not exist");
            }

            return _mapper.Map<InvitationEditReadDTO>(invitation);
        }

        [SwaggerOperation(Summary = "Answers: 0 = UNANSWERED, 1 = YES, 2 = NO, 3 = MAYBE")]
        /// <summary>
        ///  Changes the answerer for an invitation
        ///  Answers: 0 = UNANSWERED, 1 = YES, 2 = NO, 3 = MAYBE
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPut("{invitationId}")]
        public async Task<ActionResult<InvitationEditReadDTO>> ChangeInvitationAnswer(int invitationId, Answer answer)
        {

            if (!_service.EntityExists(invitationId))
            {
                return NotFound();
            }

            try
            {
                await _service.UpdateInvitationAnswer(invitationId, answer);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invitation does not exist");
            }

            Invitation invitation = await _service.GetByIdAsync(invitationId);
            return _mapper.Map<InvitationEditReadDTO>(invitation);


        }

        [SwaggerOperation(Summary = "Answers: 0 = UNANSWERED, 1 = YES, 2 = NO, 3 = MAYBE")]
        /// <summary>
        /// Endpoint for adding a invitation to the database
        /// Answers: 0 = UNANSWERED, 1 = YES, 2 = NO, 3 = MAYBE
        /// </summary>
        /// <param name="dtoInvitation">
        /// Invitation object as a InvitationCreateDTO
        /// </param>
        /// <returns>
        /// Returns the invitation
        /// </returns>
        [Authorize]
        [HttpPost()]
        public async Task<ActionResult<Invitation>> PostInvitation(InvitationCreateDTO dtoInvitation)
        {
            Invitation domainInvitation = _mapper.Map<Invitation>(dtoInvitation);

            await _service.AddAsync(domainInvitation);

            return CreatedAtAction("GetInvitationByInvitationId",
                new { invitationId = domainInvitation.InvitationId },
                _mapper.Map<InvitationEditReadDTO>(domainInvitation));
        }

        [SwaggerOperation(Summary = "Answers: 0 = UNANSWERED, 1 = YES, 2 = NO, 3 = MAYBE")]
        /// <summary>
        /// Endpoint for adding a new invitations to the database
        /// Answers: 0 = UNANSWERED, 1 = YES, 2 = NO, 3 = MAYBE
        /// </summary>
        /// <param name="id">
        /// Event Id
        /// </param>
        /// <param name="dtoInvitation">
        /// List of invitations as InviationCreateMultiDTO
        /// </param>
        /// <returns>
        /// Returns a list of invitations as InvitationEditReadDTO
        /// </returns>
        [Authorize]
        [HttpPost("{eventId}")]
        public async Task<ActionResult<List<InvitationEditReadDTO>>> PostInvitations(int eventId, List<InvitationCreateMultiDTO> dtoInvitation)
        {
            List<Invitation> domainInvitation = _mapper.Map<List<Invitation>>(dtoInvitation);
            bool createdNewInvitation = false;
            foreach(Invitation inv in domainInvitation)
            {
                try
                {
                    inv.EventId = eventId;
                    await _service.AddAsync(inv);
                    createdNewInvitation = true;
                }
                catch (UserAlreadyInvitedException)
                {
                    continue;
                }
            }
            if (!createdNewInvitation)
            {
                throw new UserAlreadyInvitedException("All your invitations already exist!");
            }
            return Created("GetInvitation",
                _mapper.Map<List<InvitationEditReadDTO>>(domainInvitation));
        }

        [SwaggerOperation(Summary = "Answers: 0 = UNANSWERED, 1 = YES, 2 = NO, 3 = MAYBE")]
        /// <summary>
        ///  Gets all the users invitations
        ///  Answers: 0 = UNANSWERED, 1 = YES, 2 = NO, 3 = MAYBE
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("user/{userId}")]
        
        public async Task<ActionResult<IEnumerable<InvitationEditReadDTO>>> GetInvitationsByUser(int userId)
        {
            return _mapper.Map<List<InvitationEditReadDTO>>(await _service.GetInvitationsForUser(userId));
        }
    }

}
