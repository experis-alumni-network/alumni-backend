﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Alumni_backend.Models.Domain;
using Alumni_backend.Models.DTOs.Comments;
using AutoMapper;

namespace Alumni_backend.Profiles
{
    public class CommentProfile : Profile
    {
        public CommentProfile()
        {
            CreateMap<Comment, CommentCreateDTO>()
                .ForMember(adto => adto.Post, opt => opt
                .MapFrom(a => a.PostId));

            CreateMap<CommentCreateDTO, Comment>()
                .ForMember(a => a.PostId, opt => opt
                .MapFrom(adto => adto.Post))
                .ForMember(a => a.Post, opt => opt
                .Ignore());

            CreateMap<Comment, CommentEditReadDTO>()
                .ForMember(adto => adto.Author, opt => opt
                .MapFrom(a => a.AuthorId))
                .ForMember(adto => adto.Post, opt => opt
                .MapFrom(a => a.PostId))
                .ReverseMap();
        }
    }
}

