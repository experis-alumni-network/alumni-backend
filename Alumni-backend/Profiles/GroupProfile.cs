﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Alumni_backend.Models.Domain;
using Alumni_backend.Models.DTOs.Groups;
using AutoMapper;

namespace Alumni_backend.Profiles
{
    public class GroupProfile : Profile
    {
        public GroupProfile()
        {
            CreateMap<Group, GroupCreateDTO>()
                .ReverseMap();


            CreateMap<Group, GroupReadDTO>()
                //Flatens Objecst To Id's
                .ForMember(gdto => gdto.GroupMembers, opt => opt
                .MapFrom(g => g.Members.Select(u => u.UserId).ToArray()))
                .ForMember(gdto => gdto.Posts, opt => opt
                .MapFrom(g => g.GroupPosts.Select(p => p.PostId).ToArray()))
                .ForMember(gdto => gdto.Admin, opt => opt
                .MapFrom(g => g.AdminId))
                .ReverseMap();
        }
    }
}
