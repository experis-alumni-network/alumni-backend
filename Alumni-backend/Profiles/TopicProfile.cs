﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Alumni_backend.Models.Domain;
using Alumni_backend.Models.DTOs.Topics;
using AutoMapper;

namespace Alumni_backend.Profiles
{
    public class TopicProfile : Profile
    {
        public TopicProfile()
        {
            CreateMap<Topic, TopicCreateDTO>()
                .ReverseMap();

            CreateMap<Topic, TopicEditReadDTO>()
                    .ForMember(adto => adto.InterestedUsers, opt => opt
                    .MapFrom(a => a.InterestedUsers.Select(m => m.UserId).ToList()))
                    .ForMember(adto => adto.Posts, opt => opt
                    .MapFrom(a => a.Posts.Select(m => m.PostId).ToList()))
                    .ReverseMap();
        }
    }
}
