﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Alumni_backend.Models.Domain;
using Alumni_backend.Models.DTOs.User;
using Alumni_backend.Models.DTOs.Users;
using AutoMapper;

namespace Alumni_backend.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserCreateDTO>()
                .ReverseMap();

            //TODO: One TO many with Post AuthorUserId
            CreateMap<User, UserReadDTO>()
                .ForMember(adto => adto.OwnedGroups, opt => opt
                .MapFrom(a => a.OwnedGroups.Select(m => m.GroupId).ToList()))
                .ForMember(adto => adto.Groups, opt => opt
                .MapFrom(a => a.Groups.Select(m => m.GroupId).ToList()))
                .ForMember(adto => adto.Topics, opt => opt
                .MapFrom(a => a.Topics.Select(m => m.TopicId).ToList()))
                .ForMember(adto => adto.AuthoredComments, opt => opt
                .MapFrom(a => a.AuthoredComments.Select(m => m.CommentId).ToList()))
                .ForMember(adto => adto.SubscribedPosts, opt => opt
                .MapFrom(a => a.SubscribedPosts.Select(m => m.PostId).ToList()))
                .ForMember(adto => adto.AuthoredPosts, opt => opt
                .MapFrom(a => a.AuthoredPosts.Select(m => m.PostId).ToList()))
                .ReverseMap();
            
            CreateMap<User, UserEditDTO>()
                    .ReverseMap();
        }
    }
}
