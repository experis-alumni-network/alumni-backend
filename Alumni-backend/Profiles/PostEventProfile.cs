﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Alumni_backend.Models.Domain;
using Alumni_backend.Models.DTOs.Posts;
using Alumni_backend.Models.DTOs.PostEvents;
using AutoMapper;

namespace Alumni_backend.Profiles
{
    public class PostEventProfile : Profile
    {
        public PostEventProfile()
        {

            CreateMap<PostEvent, PostEventReadDTO>()
                    .ForMember(adto => adto.RelatedPost, opt => opt
                    .MapFrom(a => a.RelatedPostId))
                    .ReverseMap();

            CreateMap<PostEvent, PostEventShortReadDTO>()
                .ForMember(adto => adto.RelatedPost, opt => opt
                .MapFrom(a => a.RelatedPostId))
                .ReverseMap();
        }
    }
}
