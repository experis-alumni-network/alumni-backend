﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Alumni_backend.Models.Domain;
using Alumni_backend.Models.DTOs.Posts;
using AutoMapper;

namespace Alumni_backend.Profiles
{
    public class PostProfile : Profile
    {
        public PostProfile()
        {
            CreateMap<Post, PostCreateDTO>();
                
            CreateMap<PostCreateDTO, Post>()                  
                .ForMember(a => a.GroupId, opt => opt
                .MapFrom(adto => adto.Group))
                .ForMember(a => a.Topics, opt => opt
                .MapFrom(adto => adto.Topics))
                .ForMember(a => a.Group, opt => opt
                .Ignore())
                .ForMember(a => a.Topics, opt => opt
                .Ignore());
                           
            CreateMap<Post, PostReadDTO>()
                    .ForMember(adto => adto.Author, opt => opt
                    .MapFrom(a => a.Author.UserId))
                    .ForMember(adto => adto.SubscribedUsers, opt => opt
                    .MapFrom(a => a.SubscribedUsers.Select(m => m.UserId).ToList()))
                    .ForMember(adto => adto.Topics, opt => opt
                    .MapFrom(a => a.Topics.Select(m => m.TopicId).ToList()))
                    .ForMember(adto => adto.Group, opt => opt
                    .MapFrom(a => a.GroupId))
                    .ReverseMap();

        }
    }
}
