﻿using Alumni_backend.Models.Domain;
using Alumni_backend.Models.DTOs.Invitations;
using AutoMapper;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alumni_backend.Profiles
{
    public class InvitationProfile : Profile
    {
        public InvitationProfile()
        {

            CreateMap<Invitation, InvitationCreateDTO>()
                .ForMember(adto => adto.Guest, opt => opt
                .MapFrom(a => a.GuestId))
                .ForMember(adto => adto.Event, opt => opt
                .MapFrom(a => a.EventId));

            CreateMap<InvitationCreateDTO, Invitation>()
                .ForMember(a => a.GuestId, opt => opt
                .MapFrom(adto => adto.Guest))
                .ForMember(a => a.EventId, opt => opt
                .MapFrom(adto => adto.Event))
                .ForMember(a => a.Guest, opt => opt
                .Ignore())
                .ForMember(a => a.Event, opt => opt
                .Ignore());

            CreateMap<InvitationCreateMultiDTO, Invitation>()
                .ForMember(a => a.GuestId, opt => opt
                .MapFrom(adto => adto.Guest))
                .ForMember(a => a.Guest, opt => opt
                .Ignore())
                .ForMember(a => a.Event, opt => opt
                .Ignore());


            CreateMap<Invitation, InvitationEditReadDTO>()
                .ForMember(adto => adto.Guest, opt => opt
                .MapFrom(a => a.GuestId))
                .ForMember(adto => adto.Event, opt => opt
                .MapFrom(a => a.EventId))
                .ReverseMap();

        }
    }
}
