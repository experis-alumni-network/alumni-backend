﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Alumni_backend.Models.Domain
{
    public class Group
    { 
        
        public int GroupId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        //TODO: Groups LIST OF USERS THAT ARE MEBERS OF THE GROUPE. 

        [Required]
        public bool IsPrivate { get; set; }

        
        public int AdminId { get; set; }

        [Required]
        [ForeignKey("AdminId")]
        public User Admin { get; set; }

        public ICollection<Post> GroupPosts { get; set; }
        public ICollection<User> Members { get; set; }
        

    }
}
