﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alumni_backend.Models.Domain
{
    public class Topic
    {
        public int TopicId { get; set; }
        public string Name { get; set; }
        
        public string Description { get; set; }
        
        //List of users that is interested in this topic
        public ICollection<User> InterestedUsers { get; set; }

        //List of Posts linked to a topic.
        public ICollection<Post> Posts { get; set; }

    }
}
