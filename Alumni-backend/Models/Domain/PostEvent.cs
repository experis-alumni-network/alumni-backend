﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Alumni_backend.Models.Domain
{
    public class PostEvent
    {
        public int PostEventId { get; set; }

        public int AllowedGuests { get; set; }
        
        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public string BannerImg { get; set; }

        public bool IsPrivate { get; set; }

        public int RelatedPostId { get; set; }

        [ForeignKey("RelatedPostId")]
        public Post RelatedPost { get; set; }   

        public ICollection<Invitation> Invitations { get; set; }
    }
}
