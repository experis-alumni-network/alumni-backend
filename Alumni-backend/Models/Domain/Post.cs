﻿using Alumni_backend.Models.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Alumni_backend.Models.Domain
{
    public enum PostType
    {
        VANILLA,
        EVENT
    }
    public class Post
    {

        public int PostId { get; set; }
        public string Title{ get; set; }

        [Required]
        [MaxLength(600)]
        public string Content { get; set; }
        [Required]
        public DateTime LastUpdated { get; set; }
        [Required]
        public DateTime CreationDate { get; set; }
        [Required]
        public int AuthorUserId { get; set; }

        [ForeignKey("AuthorUserId")]
        public User Author { get; set; }

        [Required]
        public PostType Type { get; set; }

        public int? GroupId { get; set; }

        [ForeignKey("GroupId")]
        public virtual Group Group { get; set; }
        
        public ICollection<Comment> Comments { get; set; }

        //Collection of Users that have subscribed to the post
        public ICollection<User> SubscribedUsers { get; set; }

        //Collection of Topics 
        public ICollection<Topic> Topics { get; set; }

    }
}
