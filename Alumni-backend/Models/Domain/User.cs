﻿using Alumni_backend.Models.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Alumni_backend.Models.Domain
{
    public class User
    {
        public int UserId { get; set; }
        [Required]
        [MaxLength(250)]
        public string Email { get; set; }

        [Required]
        [MaxLength(250)]
        public string Name { get; set; }
   
        public string ImgUrl { get; set; }

        //Curent occupation, example : Learning .net at Noroff, Backend Developer for DNB etc
        [Required]
        [MaxLength(250)]
        public string Occupation { get; set; }

        //Bio, short summary of you as a person. example: 25 year old, developer looking for a new carrer in ai.
        [Required]
        [MaxLength(250)]
        public string Bio { get; set; }

        [Required]
        [MaxLength(250)]
        public string FunFact { get; set; }


        //Provider of the auth token f.ex GOOGLE
        public string Provider { get; set; }

        public ICollection<Group> OwnedGroups { get; set; }
        public ICollection<Group> Groups { get; set; }

        //Collection of posts the user has made
        public ICollection<Post> AuthoredPosts { get; set; }

        //Collection of posts the user has subscribed to
        public ICollection<Post> SubscribedPosts { get; set; }

        //Collection of comments the user has made
        public ICollection<Comment> AuthoredComments { get; set; }
        //Collection of topics that the user is interested in.
        public ICollection<Topic> Topics { get; set; }
    }
}