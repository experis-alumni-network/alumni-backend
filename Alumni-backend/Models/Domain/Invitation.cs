﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Alumni_backend.Models.Domain
{

    public enum Answer
    {
        UNANSWERED,
        YES,
        NO,
        MAYBE
    }
    public class Invitation
    {
        public int InvitationId { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public Answer Answer { get; set; }

        public int EventId { get; set; }

        [ForeignKey("EventId")]
        public PostEvent Event { get; set; }

        public int GuestId { get; set; }

        [ForeignKey("GuestId")]
        public User Guest { get; set; }



    }
}
