﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alumni_backend.Models.DTOs.Comments
{
    public class CommentCreateDTO
    {
        public string Content { get; set; }
        public int Post { get; set; }

    }
}
