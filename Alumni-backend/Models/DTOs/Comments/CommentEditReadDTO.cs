﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alumni_backend.Models.DTOs.Comments
{
    public class CommentEditReadDTO
    {
        public int CommentId { get; set; }
        public string Content { get; set; }
        public int Post { get; set; }
        public int Author { get; set; }
    }
}
