﻿using Alumni_backend.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alumni_backend.Models.DTOs.Posts
{
    public class PostCreateDTO
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public int? Group { get; set; }
        public List<int> Topics { get; set; }

    }
}
