﻿using Alumni_backend.Models.Domain;
using Alumni_backend.Models.DTOs.Comments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alumni_backend.Models.DTOs.Posts
{
    public class PostReadDTO
    {
        public int PostId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime LastUpdated { get; set; }
        public DateTime CreationDate { get; set; }

        public PostType Type { get; set; }
        public int Author { get; set; }
        
        public int Group { get; set; }
        public ICollection<CommentEditReadDTO> Comments { get; set; }
        public int[] SubscribedUsers { get; set; }
        public int[] Topics { get; set; }
    }
}
