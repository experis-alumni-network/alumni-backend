﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alumni_backend.Models.DTOs.Posts
{
    public class PostEditDTO
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public List<int> Topics { get; set; }
    }
}
