﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alumni_backend.Models.DTOs.Invitations
{
    public class InvitationCreateMultiDTO
    {
        public int Guest { get; set; }
    }
}
