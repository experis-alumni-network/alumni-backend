﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Alumni_backend.Models.Domain;

namespace Alumni_backend.Models.DTOs.Invitations
{
    public class InvitationCreateDTO
    {

        public int Event { get; set; }

        public int Guest { get; set; }
    }
}
