﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alumni_backend.Models.DTOs.Groups
{
    public class GroupCreateDTO
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public Boolean IsPrivate { get; set; }

    }
}
