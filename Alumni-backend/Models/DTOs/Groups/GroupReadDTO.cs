﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alumni_backend.Models.DTOs.Groups
{
    public class GroupReadDTO
    {
        public int GroupId { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }

        public Boolean IsPrivate { get; set; }

        public int Admin { get; set; }
         
        public int[] GroupMembers { get; set; }
        
        public int[] Posts { get; set; }
        
    }
}
