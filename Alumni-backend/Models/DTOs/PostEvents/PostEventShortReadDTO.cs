﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alumni_backend.Models.DTOs.PostEvents
{
    public class PostEventShortReadDTO
    {
        public int PostEventId { get; set; }
        public int AllowedGuests { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public string BannerImg { get; set; }

        public bool IsPrivate { get; set; }

        public int RelatedPost { get; set; }
    }
}
