﻿using Alumni_backend.Models.Domain;
using Alumni_backend.Models.DTOs;
using Alumni_backend.Models.DTOs.Comments;
using Alumni_backend.Models.DTOs.Topics;
using Alumni_backend.Models.DTOs.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alumni_backend.Models.DTOs.PostEvents
{
    public class PostEventReadDTO
    {

        public int PostEventId { get; set; }
        public int AllowedGuests { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public string BannerImg { get; set; }

        public bool IsPrivate { get; set; }

        public int RelatedPost { get; set; }


        //POST PROPS

        public string Title { get; set; }

        public string Content { get; set; }

        public DateTime LastUpdated { get; set; }

        public DateTime CreationDate { get; set; }

        public int Author { get; set; }

        public PostType Type { get; set; }

        public int Group { get; set; }

        public ICollection<CommentEditReadDTO> Comments { get; set; }

        public List<int> SubscribedUsers { get; set; }

        public List<int> Topics { get; set; }

    }
}
