﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Alumni_backend.Models.DTOs.Users
{
    public class UserCreateDTO
    {
        public string Name { get; set; }
        public string ImgURL { get; set; }
        [Required]
        public string Email { get; set; }
        public string Occupation { get; set; }
        public string Bio { get; set; }
        public string FunFact { get; set; }
   
        public string Provider { get; set; }
    }



}
