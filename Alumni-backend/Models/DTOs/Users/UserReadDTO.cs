﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alumni_backend.Models.DTOs.User
{
    public class UserReadDTO
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public string ImgURL { get; set; }
        public string Occupation { get; set; }
        public string Bio { get; set; }
        public string FunFact { get; set; }
        public int[] AuthoredPosts { get; set; }
        public int[] SubscribedPosts { get; set; }
        public int[] AuthoredComments { get; set; }
        public int[] Topics { get; set; }
        public int[] Groups { get; set; }
        public int[] OwnedGroups { get; set; }
        public string Email { get; set; }
        public string Provider { get; set; }
    }
}
