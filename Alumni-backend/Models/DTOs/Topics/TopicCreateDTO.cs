﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alumni_backend.Models.DTOs.Topics
{
    public class TopicCreateDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
