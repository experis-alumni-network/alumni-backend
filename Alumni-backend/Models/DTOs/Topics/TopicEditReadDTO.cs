﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alumni_backend.Models.DTOs.Topics
{
    public class TopicEditReadDTO
    {
        public int TopicId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int[] InterestedUsers { get; set; }
        public int[] Posts { get; set; }
    }
}
