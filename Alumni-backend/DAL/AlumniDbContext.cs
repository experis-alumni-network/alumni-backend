﻿using Alumni_backend.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Alumni_backend.Models.Domain;
using System.IO;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Alumni_backend.DAL
{
    public class AlumniDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<PostEvent> PostEvents { get; set; }
        public DbSet<Invitation> Invitations { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Topic> Topics { get; set; }

        public AlumniDbContext(DbContextOptions<AlumniDbContext> options)
             : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            var posts = modelBuilder.Entity<Post>();

            var group = modelBuilder.Entity<Group>();

            var user = modelBuilder.Entity<User>();

            user.HasMany<Post>(u => u.AuthoredPosts).WithOne(p => p.Author).HasForeignKey(p => p.AuthorUserId).OnDelete(DeleteBehavior.ClientCascade);

            user.HasMany<Group>(u => u.OwnedGroups).WithOne(g => g.Admin).HasForeignKey(g => g.AdminId).OnDelete(DeleteBehavior.ClientCascade);

            modelBuilder.Entity<Post>()
                .HasMany(p => p.Topics)
                .WithMany(t => t.Posts)
                .UsingEntity<Dictionary<string, object>>(
                "PostTopic",
                r => r.HasOne<Topic>().WithMany().HasForeignKey("TopicId"),
                l => l.HasOne<Post>().WithMany().HasForeignKey("PostId"),
                je =>
                {
                    je.HasKey("PostId", "TopicId");
                    je.HasData(
                        new { PostId = 2, TopicId = 1 }
                        );
                });
            
            modelBuilder.Entity<Post>()
                .HasMany(p => p.Topics)
                .WithMany(t => t.Posts)
                .UsingEntity<Dictionary<string, object>>(
                "PostTopic",
                r => r.HasOne<Topic>().WithMany().HasForeignKey("TopicId"),
                l => l.HasOne<Post>().WithMany().HasForeignKey("PostId"),
                je =>
                {
                    je.HasKey("PostId", "TopicId");
                    je.HasData(
                        new { PostId = 3, TopicId = 1 }
                        );
                });

            modelBuilder.Entity<Post>()
                .HasMany(p => p.Topics)
                .WithMany(t => t.Posts)
                .UsingEntity<Dictionary<string, object>>(
                "PostTopic",
                r => r.HasOne<Topic>().WithMany().HasForeignKey("TopicId"),
                l => l.HasOne<Post>().WithMany().HasForeignKey("PostId"),
                je =>
                {
                    je.HasKey("PostId", "TopicId");
                    je.HasData(
                        new { PostId = 4, TopicId = 2 }
                        );
                });

            modelBuilder.Entity<Post>()
                .HasMany(p => p.Topics)
                .WithMany(t => t.Posts)
                .UsingEntity<Dictionary<string, object>>(
                "PostTopic",
                r => r.HasOne<Topic>().WithMany().HasForeignKey("TopicId"),
                l => l.HasOne<Post>().WithMany().HasForeignKey("PostId"),
                je =>
                {
                    je.HasKey("PostId", "TopicId");
                    je.HasData(
                        new { PostId = 5, TopicId = 2 }
                        );
                });


            user.HasMany(u => u.Groups)
                .WithMany(g => g.Members)
                .UsingEntity<Dictionary<string, object>>(
                "GroupUser",
                r => r.HasOne<Group>().WithMany().HasForeignKey("GroupId"),
                l => l.HasOne<User>().WithMany().HasForeignKey("UserId"),
                je =>
                {
                    je.HasKey("UserId", "GroupId");
                    je.HasData(
                        new { UserId = 1, GroupId = 1 }
                        ); ;
                }
                );

            user.HasMany(u => u.Groups)
                .WithMany(g => g.Members)
                .UsingEntity<Dictionary<string, object>>(
                "GroupUser",
                r => r.HasOne<Group>().WithMany().HasForeignKey("GroupId"),
                l => l.HasOne<User>().WithMany().HasForeignKey("UserId"),
                je =>
                {
                    je.HasKey("UserId", "GroupId");
                    je.HasData(
                        new { UserId = 2, GroupId = 2 }
                        ); ;
                }
                );


            modelBuilder.Entity<Comment>()
               .HasOne<User>(u => u.Author)
               .WithMany(p => p.AuthoredComments)
               .HasForeignKey(u => u.AuthorId)
               .OnDelete(DeleteBehavior.ClientCascade);

            group.HasMany(g => g.GroupPosts).WithOne(p => p.Group).HasForeignKey(p => p.GroupId);

            
               
                
               
            var user1 = new User
            {
                UserId = 1,
                Email = "henrik@gmail.com",
                Name = "Henrik Eijsink",
                ImgUrl = "https://g.acdn.no/obscura/API/dynamic/r1/ece5/tr_1000_2000_s_f/0000/osbl/2016/9/26/11/1474881616150.jpg?chk=B9ADD5",
                Bio = "This guy learning git is literally equivalent to a hippo trying to learn how to fly",
                FunFact = "Tall boi",
                Occupation = "student"
            };

            
            var user2 = new User
            {
                UserId = 2,
                Email = "theChosenOne@hogwarts.uk",
                Name = "Harry Potter",
                ImgUrl = "https://www.temashop.no/media/catalog/product/cache/cat_resized/1200/0/h/a/harry_potter_briller_tilbeh_r_briller.jpg",
                Bio = "Defeats dark lords for a living",
                FunFact = "Scar in my face was actually just me tripping and faceplanting in 3rd grade",
                Occupation = "student"
            };


            
            var user3 = new User
            {
                UserId = 3,
                Email = "books4ever@hogwarts.uk",
                Name = "Hermoine Granger",
                ImgUrl = "https://no.erch2014.com/images/iskusstvo-i-razvlecheniya/kakoe-zhe-ee-nastoyashee-imya-germiona-grejndzher.jpg",
                Bio = "Books. I read them all",
                FunFact = "I was always in love with Harry",
                Occupation = "student"
            };

            var user4 = new User
            {
                UserId = 4,
                Email = "ronnyw@hogwarts.uk",
                Name = "Ron Weasley",
                ImgUrl = "https://static.wikia.nocookie.net/hpmor/images/8/83/Ron_Weasley.jpeg/revision/latest/top-crop/width/360/height/450?cb=20190217164742",
                Bio = "I love spiders",
                FunFact = "Can't properly pronounce Wingardium Leviosa",
                Occupation = "student"
            };

            var user5 = new User
            {
                UserId = 5,
                Email = "ablus@hogwarts.uk",
                Name = "Albus Dumbledore",
                ImgUrl = "https://www.looper.com/img/gallery/dumbledores-backstory-explained/intro-1567606121.jpg",
                Bio = "Running out of creativity",
                FunFact = "Has a burning bird",
                Occupation = "Headmaster"
            };

            var user6 = new User
            {
                UserId = 6,
                Email = "harrypottersucks@hogwarts.uk",
                Name = "Draco Malfoy",
                ImgUrl = "https://pbs.twimg.com/profile_images/1298380607068745729/t65sdSut.jpg",
                Bio = "I am the best",
                FunFact = "I love my daddy",
                Occupation = "student"
            };

            var topic1 = new Topic
            {
                TopicId = 1,
                Description = "The future",
                Name = "AI"
            };

            var topic2 = new Topic
            {
                TopicId = 2,
                Description = ".NET stuff",
                Name = ".NET"
            };


            var group1 = new Group
            {
                GroupId = 1,
                Name = "Noroff Summer 2021",
                Description = "Group for noroff 2021",
                IsPrivate = false,
                AdminId = 1,
            };

            var group2 = new Group
            {
                GroupId = 2,
                Name = "Alumni-Dev",
                Description = "Developers for alumni stuff",
                IsPrivate = false,
                AdminId = 2,
            };

            var post1 = new Post
            {
                PostId = 1,
                Title = "Why is git so weird to use, sometimes?",
                Content = "Sometimes i strugle with Git, i dont know why. Any help?",
                LastUpdated = DateTime.ParseExact("15/06/2021 13:45:00", "dd/MM/yyyy HH:mm:ss", null),
                CreationDate = DateTime.ParseExact("15/06/2015 13:45:00", "dd/MM/yyyy HH:mm:ss", null),
                Type = PostType.VANILLA,
                AuthorUserId = 1,
                GroupId = 1
            };


            var post2 = new Post
            {
                PostId = 2,
                Title = "Learn to use Ai to autocomplete code.",
                Content = "Welcome to a day where we are learning to use Ai, to reduce the amount of code we need to wright",
                LastUpdated = DateTime.ParseExact("15/06/2021 13:45:00", "dd/MM/yyyy HH:mm:ss", null),
                CreationDate = DateTime.ParseExact("15/06/2015 13:45:00", "dd/MM/yyyy HH:mm:ss", null),
                Type = PostType.EVENT,
                AuthorUserId = 3,
                GroupId = 1,
                
            };

            
            var post3 = new Post
            {
                PostId = 3,
                Title = "Why Ai is the future of teaching",
                Content = "We should just use Ai, am i right",
                LastUpdated = DateTime.ParseExact("15/06/2021 13:45:00", "dd/MM/yyyy HH:mm:ss", null),
                CreationDate = DateTime.ParseExact("15/06/2015 13:45:00", "dd/MM/yyyy HH:mm:ss", null),
                Type = PostType.VANILLA,
                AuthorUserId = 1
            };

            var post4 = new Post
            {
                PostId = 4,
                Title = "Why i believe .net is bether then Java",
                Content = "I believe .net is bether then java, because i like it bether!!",
                LastUpdated = DateTime.ParseExact("21/10/2021 08:00:00", "dd/MM/yyyy HH:mm:ss", null),
                CreationDate = DateTime.ParseExact("21/10/2021 08:10:00", "dd/MM/yyyy HH:mm:ss", null),
                Type = PostType.VANILLA,
                AuthorUserId = 6
            };

            var post5 = new Post
            {
                PostId = 5,
                Title = "Gathering of .net developers. NOT JAVA",
                Content = "This is a gathering for us developers that love .net, not JAVA!!",
                LastUpdated = DateTime.ParseExact("21/10/2021 09:00:00", "dd/MM/yyyy HH:mm:ss", null),
                CreationDate = DateTime.ParseExact("21/10/2021 09:10:00", "dd/MM/yyyy HH:mm:ss", null),
                Type = PostType.EVENT,
                AuthorUserId = 6,
            };

            var comment1 = new Comment
            {
                CommentId = 1,
                Content = "AGREED",
                PostId = 1,
                AuthorId = 3
            };



            modelBuilder.Entity<User>()
               .HasData(new User[] {
                    user1, 
                    user2,
                    user3,
                    user4,
                    user5,
                    user6
               });


            var post50 = new Post
            {
                PostId = 50,
                Title = "Hell's Kitchen watch party",
                Content = "Come to my Hell's Kitchen watch party you idiot sandwiches!",
                LastUpdated = DateTime.ParseExact("15/06/2021 13:45:00", "dd/MM/yyyy HH:mm:ss", null),
                CreationDate = DateTime.ParseExact("15/06/2015 13:45:00", "dd/MM/yyyy HH:mm:ss", null),
                Type = PostType.EVENT,
                AuthorUserId = 1
            };

            modelBuilder.Entity<PostEvent>()
                .HasData(new PostEvent[]
                {
                    new PostEvent
                    {
                        PostEventId = 1,
                        AllowedGuests = 25,
                        StartTime = DateTime.Now,
                        EndTime = DateTime.Now,
                        BannerImg = "https://www.sciencenewsforstudents.org/wp-content/uploads/2021/08/LL_AI_feat-1030x580.jpg",
                        IsPrivate = false,
                        RelatedPostId = 2
                    },
                    new PostEvent
                    {
                        PostEventId = 3,
                        AllowedGuests = 200,
                        StartTime = DateTime.Now,
                        EndTime = DateTime.Now,
                        BannerImg = "https://upload.wikimedia.org/wikipedia/commons/a/a3/.NET_Logo.svg",
                        IsPrivate = false,
                        RelatedPostId = 5,
                    },
                    new PostEvent
                    {
                        PostEventId = 4,
                        AllowedGuests = 200,
                        StartTime = DateTime.Now,
                        EndTime = DateTime.Now,
                        BannerImg = "https://i.pinimg.com/originals/bc/34/61/bc3461ec3ed85a9822ee90be77ff48fc.jpg",
                        IsPrivate = false,
                        RelatedPostId = 50,
                    },
                });

            var converter = new EnumToStringConverter<Answer>();

            modelBuilder
                .Entity<Invitation>()
                .Property(e => e.Answer)
                .HasConversion(converter);

            modelBuilder.Entity<Invitation>()
                .HasData(new Invitation[]
                {
                    new Invitation
                    {
                        InvitationId = 1,
                        Answer = Answer.YES,
                        EventId = 1,
                        GuestId = 1
                    },
                    new Invitation
                    {
                        InvitationId = 2,
                        Answer = Answer.NO,
                        EventId = 1,
                        GuestId = 2
                    }
                }
                );

            modelBuilder.Entity<Topic>()
                .HasData(new Topic[]
                {
                    topic1,
                    topic2
                });
        

            modelBuilder.Entity<Post>()
                .HasData(new Post[]
                {
                    post1,
                    post2,
                    post3,
                    post4,
                    post5,
                    post50
                });

            modelBuilder.Entity<Comment>()
               .HasData(new Comment[]
               {
                    comment1
               });

            modelBuilder.Entity<Group>()
               .HasData(new Group[]
               {
                   group1,
                   group2
               });
            

        }

    }
}
