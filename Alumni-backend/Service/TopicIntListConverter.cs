﻿using Alumni_backend.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alumni_backend.Service
{
    public static class TopicIntListConverter
    {
        public static async Task<List<Models.Domain.Topic>> IntListToTopics(List<int> intList, AlumniDbContext context)
        {
            if (intList == null)
            {
                throw new ArgumentException("List is empty");
            }
            TopicService topicService = new TopicService(context);
            List<Models.Domain.Topic> topics = new List<Models.Domain.Topic>();
            foreach (int topicId in intList)
            {
                try
                {
                    topics.Add(await topicService.GetByIdAsync(topicId));
                }
                catch
                {
                    throw new ArgumentException("Topic does not exist");
                }

            }
            return topics;
        }
    }
}
