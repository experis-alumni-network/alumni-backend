﻿using Alumni_backend.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alumni_backend.Service
{
    public interface IPostEventService<T>
    {
        public Task<IEnumerable<User>> GetInvitedGuestsForEventAsync(int eventid);

        public Task<IEnumerable<User>> GetAllAcceptedGuestsForEventAsync(int eventId);

        public Task<IEnumerable<User>> GetAllDeclinedUsersForEventAsync(int eventId);

        public Task<IEnumerable<User>> GetAllUnansweredUsersForEventAsync(int eventId);

        public Task<IEnumerable<User>> GetAllMaybeUsersForEventAsync(int eventId);

        public Task<IEnumerable<PostEvent>> GetAllHostedEventsForUserAsync(int userId);

        public Task<IEnumerable<PostEvent>> GetUpcommingEventsForUserAsync(int userId, int months);

        public Task<IEnumerable<T>> GetAllAsync();
        public Task<T> GetByPostIdAsync(int postId);

        public Task AddAsync(PostEvent postEvent);
        public Task DeleteAsync(int id);
        public Task UpdateAsync(PostEvent postEvent, Post post);
        public bool EntityExists(int id);

        public Task<T> GetByPostIdShortAsync(int id);
    }
}
