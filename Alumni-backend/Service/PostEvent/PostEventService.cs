﻿using Alumni_backend.DAL;
using Alumni_backend.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Alumni_backend.Service
{
    public class PostEventService : IPostEventService<PostEvent>
    {
        public readonly AlumniDbContext _context;

        public PostEventService(AlumniDbContext context)
        {
            _context = context;
        }
        //TODO REMOVE LATER
        public async Task AddAsync(PostEvent postEvent)
        {
            _context.Posts.Add(postEvent.RelatedPost);
            _context.PostEvents.Add(postEvent);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var postEvent = await _context.PostEvents.FindAsync(id);
            _context.PostEvents.Remove(postEvent);
            await _context.SaveChangesAsync();

        }

        public bool EntityExists(int id)
        {
            return _context.PostEvents.Any(e => e.PostEventId == id);
        }

        public async Task<IEnumerable<User>> GetAllAcceptedGuestsForEventAsync(int eventId)
        {
            List<User> acceptedUsers = new List<User>();
            var acceptedInvitations = await _context.Invitations
             .Where(i => i.EventId == eventId && i.Answer == Answer.YES)
             .ToListAsync();

            acceptedInvitations.ForEach(a => acceptedUsers.Add(_context.Users.Find(a.GuestId)));
            return acceptedUsers;
        }

        public async Task<IEnumerable<PostEvent>> GetAllAsync()
        {

            return await _context.PostEvents
                .Include(p => p.RelatedPost)
                .ThenInclude(p => p.Comments)
                .Include(p => p.RelatedPost)
                .ThenInclude(p => p.SubscribedUsers)
                .Include(p => p.RelatedPost)
                .ThenInclude(p => p.Topics)
                .ToListAsync();

        }

        public async Task<IEnumerable<User>> GetAllDeclinedUsersForEventAsync(int eventId)
        {
            List<User> declinedUsers = new List<User>();
            var declinedInvitations = await _context.Invitations
             .Where(i => i.EventId == eventId && i.Answer == Answer.NO)
             .ToListAsync();

            declinedInvitations.ForEach(a => declinedUsers.Add(_context.Users.Find(a.GuestId)));
            return declinedUsers;
        }


        public Task<IEnumerable<PostEvent>> GetAllHostedEventsForUserAsync(int userId)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<User>> GetInvitedGuestsForEventAsync(int eventId)
        {
            List<User> allInvitedUsers = new List<User>();
            var invitations = await _context.Invitations
             .Where(i => i.EventId == eventId)
             .ToListAsync();

            invitations.ForEach(a => allInvitedUsers.Add(_context.Users.Find(a.GuestId)));
            return allInvitedUsers;

        }

        public async Task<IEnumerable<User>> GetAllMaybeUsersForEventAsync(int eventId)
        {
            List<User> maybeUsers = new List<User>();
            var maybeInvitations = await _context.Invitations
             .Where(i => i.EventId == eventId && i.Answer == Answer.MAYBE)
             .ToListAsync();

            maybeInvitations.ForEach(a => maybeUsers.Add(_context.Users.Find(a.GuestId)));
            return maybeUsers;
        }

        public async Task<IEnumerable<User>> GetAllUnansweredUsersForEventAsync(int eventId)
        {
            List<User> unansweredUsers = new List<User>();
            var unansweredInvitations = await _context.Invitations
             .Where(i => i.EventId == eventId && i.Answer == Answer.UNANSWERED)
             .ToListAsync();

            unansweredInvitations.ForEach(a => unansweredUsers.Add(_context.Users.Find(a.GuestId)));
            return unansweredUsers;
        }


        public async Task<PostEvent> GetByPostIdAsync(int postId)
        {
            return await _context.PostEvents
                .Include(p => p.RelatedPost)
                .ThenInclude(p => p.Comments)          
                .Include(p => p.RelatedPost)
                .ThenInclude(p => p.SubscribedUsers)
                .Include(p => p.RelatedPost)
                .ThenInclude(p => p.Topics)
                .Where(p => p.RelatedPostId == postId)
                .SingleOrDefaultAsync();
        }

        public async Task UpdateAsync(PostEvent postEvent, Post post)
        {
            _context.Entry(post).State = EntityState.Modified;
            _context.Entry(postEvent).State = EntityState.Modified;
            await _context.SaveChangesAsync();

        }

        public async Task<IEnumerable<PostEvent>> GetUpcommingEventsForUserAsync(int userId, int months = 0)
        {
            var invitations = await _context.Invitations
                .Where(i => i.GuestId == userId)
                .ToListAsync();

            List<PostEvent> events = new List<PostEvent>();
            foreach (Invitation invitation in invitations)
            {
                var evnt = await _context.PostEvents
                .Include(p => p.RelatedPost)
                .ThenInclude(p => p.Comments)
                .Include(p => p.RelatedPost)
                .ThenInclude(p => p.SubscribedUsers)
                .Include(p => p.RelatedPost)
                .ThenInclude(p => p.Topics)
                .Where(p => p.PostEventId == invitation.EventId)
                .SingleOrDefaultAsync();

                if (evnt.StartTime > DateTime.Now.AddMonths(months) || months == 0){
                    events.Add(evnt);
                }

            }
            return events;
        }

        public async Task<PostEvent> GetByPostIdShortAsync(int postId)
        {
            return await _context.PostEvents
                .Where(p => p.RelatedPostId == postId)
                .SingleOrDefaultAsync();
        }
    }
}
