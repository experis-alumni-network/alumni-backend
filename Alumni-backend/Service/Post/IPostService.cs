﻿using Alumni_backend.DAL;
using Alumni_backend.Models.Domain;
using Alumni_backend.Models.DTOs.Posts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alumni_backend.Service
{
    public interface IPostService : IService<Post>
    {
        public Task UpdatePostAsync(int id, PostEditDTO post);
        public Task<Post> AddPostAsync(Post post, List<int> topics);

        public Task<ICollection<Post>> GetAllPostedByUser(int userId);
        public Task<ICollection<Post>> GetAllPostsAsync(User user);
        
    }
}
