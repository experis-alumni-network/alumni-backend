using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Alumni_backend.DAL;
using Alumni_backend.Models.Domain;
using Alumni_backend.Models.DTOs;
using Alumni_backend.Models.DTOs.Posts;
using Alumni_backend.Service;
using Microsoft.EntityFrameworkCore;

namespace Alumni_backend.Service
{
    public class PostService : IPostService
    {
        public readonly AlumniDbContext _context;

        public PostService(AlumniDbContext context)
        {
            _context = context;
        }


        public async Task<Post> AddAsync(Post post)
        {
           
            _context.Posts.Add(post);
            await _context.SaveChangesAsync();
            return post;
        }

        public async Task DeleteAsync(int id)
        {
            var post = await _context.Posts.FindAsync(id);
            _context.Posts.Remove(post);
            await _context.SaveChangesAsync();

        }
        
        public async Task<IEnumerable<Post>> GetAllAsync()
        {
            return await _context.Posts.Include(p => p.Comments).Include(p => p.Author).Include(p => p.Topics).Include(p => p.Group).ToListAsync();
        }

        public  async Task<ICollection<Post>> GetAllPostedByUser(int userId)
        {
            return await _context.Posts.Include(p => p.Comments).Include(p => p.Author).Include(p => p.Topics).Include(p => p.Group).Where(p => p.AuthorUserId == userId).ToListAsync();
        }

        
        public async Task<ICollection<Post>> GetAllRelevantPosts(int userId)
        {
            List<Post> temp = new();

            User user = await _context.Users
                .Include(u => u.Groups)
                .Include(u => u.Topics)
                .Include(u => u.AuthoredPosts).ThenInclude(p => p.Topics)
                .Include(u => u.AuthoredPosts).ThenInclude(p => p.Author)
                .FirstOrDefaultAsync(u => u.UserId == userId);

            foreach(Topic tp in user.Topics)
            {
                Topic tempTopic = await _context.Topics
                    .Include(t => t.Posts).ThenInclude(p => p.Group)
                    .Include(t => t.Posts).ThenInclude(t => t.Topics)
                    .Include(t => t.Posts).ThenInclude(p => p.Author)
                    .FirstOrDefaultAsync(t => t.TopicId == tp.TopicId);

                foreach(Post p in tempTopic.Posts)
                {
                    if(p.Group == null)
                    {
                        temp.Add(p);
                    }
                    else
                    {
                        if(p.Group.IsPrivate == true)
                        {
                            if (user.Groups.Contains(p.Group) == true)
                            {
                                temp.Add(p);
                            }
                            //Do nothing
                        }
                        else
                        {
                            temp.Add(p);
                        }
                         
                    }
                }
            }

            foreach(Group gr in user.Groups)
            {
                //Includes the posts
                Group tempGroup = await _context.Groups
                    .Include(g => g.GroupPosts).ThenInclude(p => p.Topics)
                    .Include(g => g.GroupPosts).ThenInclude(p => p.Author)
                    .FirstOrDefaultAsync(g => g.GroupId == gr.GroupId);

                foreach(Post p in tempGroup.GroupPosts)
                {
                    if (temp.Contains(p) == false)
                    {
                        temp.Add(p);
                    }
                    else
                    {
                        //do nothing
                    }
                }
            }

            foreach(Post p in user.AuthoredPosts)
            {
                if(temp.Contains(p) == false)
                {
                    temp.Add(p);
                }
            }

            return temp;
        }

        public async Task<ICollection<Post>> GetAllPostsAsync(User currentUser)
        {
            List<Post> temp = await _context.Posts.Include(p => p.Comments).Include(p => p.Author).Include(p => p.Topics).Include(p => p.Group).ToListAsync();

            List<Post> returnList = new();

            foreach(Post post in temp)
            {
                if(post.Group == null)
                {
                    returnList.Add(post);
                }
                else
                {
                    if(post.Group.IsPrivate == true)
                    {
                        Models.Domain.Group groupToCheck = await _context.Groups.Include(g => g.Members).FirstOrDefaultAsync(g => g.GroupId == post.GroupId);
                        if (groupToCheck.Members.Contains(currentUser))
                        {
                            returnList.Add(post);
                        }
                        //else do nothing
                    }
                    else
                    {
                        returnList.Add(post);
                    }
                }
            }
            return returnList;
        }

        public virtual async Task<Post> GetByIdAsync(int id)
        {
            return await _context.Posts.Include(p => p.Comments).Include(p => p.Author).Include(p => p.Topics).Where(p => p.PostId == id).FirstAsync();
        }

        public async Task UpdatePostAsync(int id, PostEditDTO postDto)
        {
            Post postToUpdate = await _context.Posts.FindAsync(id);
            

            postToUpdate.Title = postDto.Title;
            postToUpdate.Content = postDto.Content;
            if(postDto.Topics != null)
            {
                postToUpdate.Topics = await TopicIntListConverter.IntListToTopics(postDto.Topics, _context);
            }
            postToUpdate.LastUpdated = DateTime.Now;

            await _context.SaveChangesAsync();
        }

        public bool EntityExists(int id)
        {
            return _context.Posts.Any(e => e.PostId == id);
        }

        public Task UpdateAsync(Post entity)
        {
            throw new NotImplementedException();
        }

        public async Task<Post> AddPostAsync(Post post, List<int> topics)
        {
            List<Topic> temp = new();

            if (topics != null )
            {
                foreach (int t in topics)
                {
                    var a = await _context.Topics.FindAsync(t);
                    temp.Add(a);
                    Console.WriteLine(a);
                }
            }

            post.Topics = temp;

            _context.Posts.Add(post);
            await _context.SaveChangesAsync();
            return post;
        }
    }
}   
