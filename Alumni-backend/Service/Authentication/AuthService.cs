﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Alumni_backend.DAL;
using Alumni_backend.Models.Domain;

namespace Alumni_backend.Service.Authentication
{
    public class AuthService
    {
        public async static Task<User> GetLoggedInUser(ClaimsIdentity identity, AlumniDbContext context)
        {
            UserService userService = new UserService(context);
            if (identity.IsAuthenticated)
            {
                IEnumerable<Claim> claims = identity.Claims;
                string email = claims
                    .Where(x => x.Type == ClaimTypes.Email)
                    .FirstOrDefault().Value;

                User loggedInUser = await userService.GetByEmailAsync(email);
                return loggedInUser;

            }

            //TODO Change return value to null, make error handling at endpoints to Bad Request
            User a = await userService.GetByEmailAsync("henrik@gmail.com");
            return a;
        }
    }
}
