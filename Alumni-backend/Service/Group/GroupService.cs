﻿using Alumni_backend.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using Alumni_backend.Models.Domain;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Alumni_backend.Models.DTOs.Groups;

namespace Alumni_backend.Service
{
    /// <summary>
    /// Service for group. Implements the IGroupService
    /// </summary>
    public class GroupService : IGroupService
    {
        public readonly AlumniDbContext _context;

        public GroupService(AlumniDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Function for adding a group to the database
        /// </summary>
        /// <param name="group">
        /// Group Object 
        /// </param>
        /// <returns>
        /// Returns the group
        /// </returns>
        public async Task<Models.Domain.Group> AddAsync(Models.Domain.Group group)
        {
            _context.Groups.Add(group);
            await _context.SaveChangesAsync();
            return group;
        }

        /// <summary>
        /// Function for deleting a group from the database
        /// </summary>
        /// <param name="id">
        /// Group ID
        /// </param>
        public async Task DeleteAsync(int id)
        {
            var group = await _context.Groups.FindAsync(id);
            _context.Groups.Remove(group);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Helper function for checking if a group exists
        /// </summary>
        /// <param name="id">
        /// Group Id
        /// </param>
        /// <returns>
        /// Returns the group
        /// </returns>
        public bool EntityExists(int id)
        {
            return _context.Groups.Any(e => e.GroupId == id);
        }

        /// <summary>
        /// Function for retrieving all groups from the database
        /// </summary>
        /// <returns>
        /// Returns a list of groups
        /// </returns>
        public async Task<IEnumerable<Models.Domain.Group>> GetAllAsync()
        {
            return await _context.Groups
                .Include(g => g.Members)
                .Include(g => g.GroupPosts)
                .ToListAsync();
        }

        /// <summary>
        /// Function for getting all Groups where a user is a member.
        /// </summary>
        /// <param name="userId">
        /// User Id
        /// </param>
        /// <returns>
        /// Returns a list of group's
        /// </returns>
        public async Task<ICollection<Models.Domain.Group>> GetAllGroupsForUser(int userId)
        {
            List<Models.Domain.Group> groups = await _context.Groups.Include(g => g.Members).ToListAsync();

            User user = await _context.Users.FindAsync(userId);

            List<Models.Domain.Group> temp = new();

            foreach (Models.Domain.Group group in groups)
            {
                if (group.IsPrivate)
                {
                    if (group.Members.Contains(user))
                    {
                        temp.Add(group);
                        
                    }
                }
                else
                {
                    temp.Add(group);
                }
                
            }
            return temp;
        }

        //Check if member Is int he group if it is private.

        /// <summary>
        /// Function for retrieving a single group.
        /// </summary>
        /// <param name="id">
        /// Group id
        /// </param>
        /// <returns>
        /// Returns a group
        /// </returns>
        public async Task<Models.Domain.Group> GetByIdAsync(int id)
        {
            return await _context.Groups
                .Include(g => g.Members)
                .Include(g => g.GroupPosts)
                .FirstOrDefaultAsync(i => i.GroupId == id);
        }

        /// <summary>
        /// Function for retrieving posts in a group
        /// </summary>
        /// <param name="id">
        /// Group Id
        /// </param>
        /// <returns>
        /// Returns a list of posts
        /// </returns>
        public async Task<ICollection<Post>> GetPostsForGroup(int id)
        {
            Models.Domain.Group group = await _context.Groups.Include(g => g.GroupPosts).ThenInclude(p => p.Author).Where(g => g.GroupId == id).FirstAsync();

            List<Post> posts = new();
            foreach(Post post in group.GroupPosts) 
            {
                posts.Add(post);
            }
            return posts;
        }

        /// <summary>
        /// Function for retrieving a list of users in a group
        /// </summary>
        /// <param name="groupId">
        /// Group Id</param>
        /// <returns>
        /// Returns a list of Users
        /// </returns>
        public async Task<IEnumerable<User>> GetUsersInGroup(int groupId)
        {
            Models.Domain.Group group = await _context.Groups
                .Include(g => g.Members)
                .Where(g => g.GroupId == groupId)
                .FirstAsync();

            List<User> returnUsers = group.Members.ToList();


            return returnUsers;
        }

        /// <summary>
        /// Function for adding one or more users to a group. Used when adding users to a private group. Only members allready in the group should be able to add new members to a private group
        /// </summary>
        /// <param name="groupId">
        /// Group Id
        /// </param>
        /// <param name="ids">
        /// List of User Id's
        /// </param>
        public async Task InviteUsersToGroup(int groupId, List<int> ids)
        {
            Models.Domain.Group groupToUpdate = await _context.Groups.Include(g => g.Members).Where(g => g.GroupId == groupId).FirstAsync();
            foreach (int id in ids)
            {
                User user = await _context.Users.FindAsync(id);
                if (!groupToUpdate.Members.Any(u => u.UserId == user.UserId))
                {
                    groupToUpdate.Members.Add(user);
                }
                
            }
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Function for joining a group. Wil be used when a user wants to join a public group.
        /// </summary>
        /// <param name="groupId">
        /// Group Id</param>
        /// <param name="user">
        /// User object, currently logged in user.
        /// </param>  
        public async Task JoinGroup(int groupId, User user)
        {
            Models.Domain.Group groupToUpdate = await _context.Groups.Include(g => g.Members).Where(g => g.GroupId == groupId).FirstAsync();

            List<User> temp = new();

            foreach(User u in groupToUpdate.Members)
            {
                temp.Add(u);
            }
            temp.Add(user);

            groupToUpdate.Members = temp;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Function for Updating a group.
        /// </summary>
        /// <param name="id">
        /// Group Id
        /// </param>
        /// <param name="groupDTO">
        /// GroupEditDTO
        /// </param>
        public async Task UpdateGroupAsync(int id, GroupEditDTO groupDTO)
        {
            Models.Domain.Group groupToUpdate = await _context.Groups.FindAsync(id);


            groupToUpdate.Name = groupDTO.Name;
            groupToUpdate.Description = groupDTO.Description;
            groupToUpdate.IsPrivate = groupDTO.IsPrivate;

            await _context.SaveChangesAsync();
        }

    }

}
