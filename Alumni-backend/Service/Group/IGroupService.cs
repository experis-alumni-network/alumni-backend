﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Alumni_backend.Models.Domain;
using Alumni_backend.Models.DTOs.Groups;

namespace Alumni_backend.Service
{
    /// <summary>
    /// Interface for Group. Implements the IService interface.
    /// This interface provides functions that are specific for a group.
    /// </summary>
    public interface IGroupService : IService<Models.Domain.Group>
    {
        public Task<ICollection<Post>> GetPostsForGroup(int id);
        public Task<ICollection<Models.Domain.Group>> GetAllGroupsForUser(int id);
        public Task UpdateGroupAsync(int groupId, GroupEditDTO dto);
        public Task InviteUsersToGroup(int groupId, List<int> ids);

        public Task JoinGroup(int groupId, User user);

        public Task<IEnumerable<User>> GetUsersInGroup(int groupId);


    }
}
