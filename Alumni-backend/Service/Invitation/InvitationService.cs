﻿using Alumni_backend.DAL;
using Alumni_backend.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Alumni_backend.Service
{
    public class InvitationService : IInvitationService<Invitation>
    {
        public readonly AlumniDbContext _context;

        public InvitationService(AlumniDbContext context)
        {
            _context = context;
        }

        public async Task<Invitation> AddAsync(Invitation entity)
        {
            if(_context.Invitations.Count((a) => a.GuestId == entity.GuestId && a.EventId == entity.EventId) != 0){
                throw new UserAlreadyInvitedException("Invitation already exists");
            }

            _context.Invitations.Add(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task DeleteAsync(int id)
        {
            var movie = await _context.Invitations.FindAsync(id);
            _context.Invitations.Remove(movie);
            await _context.SaveChangesAsync();

        }

        public bool EntityExists(int id)
        {
            return _context.Invitations.Any(e => e.InvitationId == id);
        }

        public async Task<IEnumerable<Invitation>> GetAllAsync()
        {
            return await _context.Invitations
                .ToListAsync();
        }

        public async Task<Invitation> GetInvitationByEventIdAndUserId(int eventId, int userId)
        {
            Invitation invitation = new Invitation();
            try
            {
                invitation = await _context.Invitations.Where(i => i.EventId == eventId && i.GuestId == userId).FirstAsync();
            }
            catch(InvalidOperationException)
            {
                
            }
            return invitation;
        }

        public async Task<IEnumerable<Invitation>> GetInvitationsForUser(int id)
        {
            return await _context.Invitations.Where(i => i.GuestId == id)
                .ToListAsync();

        }

        public async Task<Invitation> GetByIdAsync(int Id)
        {
            return await _context.Invitations.FindAsync(Id);
        }

        public async Task UpdateAsync(Invitation entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();

        }

        public async Task UpdateInvitationAnswer(int id, Answer answer)
        {
            Invitation invitationToUpdate = await _context.Invitations
             .FindAsync(id);

            invitationToUpdate.Answer = answer;
            await _context.SaveChangesAsync();



        }
    }
}