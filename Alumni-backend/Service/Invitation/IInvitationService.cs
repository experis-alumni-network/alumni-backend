﻿using Alumni_backend.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alumni_backend.Service
{
    public interface IInvitationService<T> : IService<T>
    {
        public Task<IEnumerable<T>> GetInvitationsForUser(int id);

        public Task<Invitation> GetInvitationByEventIdAndUserId(int eventId, int userId);
    }
}
