﻿using System;
using Alumni_backend.Models.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alumni_backend.Service
{
    /// <summary>
    /// Interface for Topic, implements the IService interface.
    /// This interface provides functions that are specific for a topic.
    /// </summary>
    public interface ITopicService : IService<Topic>
    {
        
        public Task<ICollection<Post>> GetAllPostFromTopic(User user, int topicId);
        
        public Task<Topic> SubscribeToTopic(User user, int topciId);

    }
}
