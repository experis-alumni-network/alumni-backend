﻿using Alumni_backend.DAL;
using Alumni_backend.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Alumni_backend.Service
{
    /// <summary>
    /// Service for Topic's. Implements the ITopicService. 
    /// </summary>
    public class TopicService : ITopicService
    {
        public readonly AlumniDbContext _context;

        public TopicService(AlumniDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Function for retrieving all Topics from the database.
        /// </summary>
        /// <returns>
        /// Returns a list of Topics.
        /// </returns>
        public async Task<IEnumerable<Topic>> GetAllAsync()
        {
            return await _context.Topics
                .Include(t => t.Posts)
                .Include(t => t.InterestedUsers)
                .ToListAsync();
        }

        /// <summary>
        /// Function for retrieving a single Topic from the database
        /// </summary>
        /// <param name="id">
        /// Topic ID
        /// </param>
        /// <returns>
        /// Returns a single Topic
        /// </returns>
        public async Task<Topic> GetByIdAsync(int id)
        {
            return await _context.Topics
                .Include(t => t.Posts)
                .Include(t => t.InterestedUsers)
                .FirstOrDefaultAsync(i => i.TopicId == id);
        }


        /// <summary>
        /// Function for adding a topic to the database
        /// </summary>
        /// <param name="topic">
        /// Topic object.
        /// </param>
        /// <returns>
        /// Returns the topic
        /// </returns>
        public async Task<Topic> AddAsync(Topic topic)
        {
            _context.Topics.Add(topic);
            await _context.SaveChangesAsync();
            return topic; 
        }

        /// <summary>
        /// Function for deleting a topic from the database
        /// </summary>
        /// <param name="id">
        /// Topic Id
        /// </param>
        public async Task DeleteAsync(int id)
        {
            var topic = await _context.Topics.FindAsync(id);
            _context.Topics.Remove(topic);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Function for updating a topic in the database
        /// </summary>
        /// <param name="topic">
        /// Topic object.
        /// </param>
        public async Task UpdateAsync(Topic topic)
        {
            _context.Entry(topic).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Helper function for checking if a topic allready exists
        /// </summary>
        /// <param name="id">
        /// Topic Id
        /// </param>
        /// <returns>
        /// Returns the Topic
        /// </returns>
        public bool EntityExists(int id)
        {
            return _context.Topics.Any(e => e.TopicId == id);
        }

        /// <summary>
        /// Function for retrieving all posts from a topic. Filters out posts that are posted in private group's that the user is not a member of. 
        /// </summary>
        /// <param name="currentUser">
        /// User object, currently logged inn user provided by the authservice. 
        /// </param>
        /// <param name="topicId">
        /// Topic Id
        /// </param>
        /// <returns>
        /// Returns a list of posts.
        /// </returns>
        public async Task<ICollection<Post>> GetAllPostFromTopic(User currentUser, int topicId)
        {

            Topic currentTopic = await _context.Topics
                .Include(topic => topic.Posts).ThenInclude(post => post.Topics)
                .Include(topic => topic.Posts).ThenInclude(post => post.Group)
                .Include(topic => topic.Posts).ThenInclude(post => post.Author)
                .FirstOrDefaultAsync(t => t.TopicId == topicId);

            List<Post> temp = new();

            if (currentTopic.Posts != null)
            {
                
                foreach (Post post in currentTopic.Posts)
                {

                    //Checks if the group for the current post contains the given user
                    if(post.Group == null)
                    {
                        temp.Add(post);
                    }
                    else
                    {
                        if (post.Group.IsPrivate == true)
                        {
                            Models.Domain.Group groupToCheck = await _context.Groups.Include(g => g.Members).FirstOrDefaultAsync(g => g.GroupId == post.GroupId);
                            if (groupToCheck.Members.Contains(currentUser))
                            {
                                temp.Add(post);
                            }
                            //else do nothing
                        }
                        else
                        {
                            temp.Add(post);
                        }
                    }
                }
            }

            return temp; 
        }

        /// <summary>
        /// Function for subscribing to a topic, adds the currently logged in user to the topic's InterestedUsers.
        /// </summary>
        /// <param name="currentUser">
        /// User object
        /// </param>
        /// <param name="topciId">
        /// Topic Id
        /// </param>
        /// <returns>
        /// Returns the topic. 
        /// </returns>
        public async Task<Topic> SubscribeToTopic(User currentUser, int topciId)
        {
            Topic topicToUpdate = await _context.Topics.Include(t => t.InterestedUsers).Where(t => t.TopicId == topciId).FirstAsync();

            topicToUpdate.InterestedUsers.Add(currentUser);
             
            await _context.SaveChangesAsync();
            return topicToUpdate;

        }
    }
}
