﻿using Alumni_backend.DAL;
using Alumni_backend.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;


namespace Alumni_backend.Service
{
    public class CommentService : ICommentService<Comment>
    {
        public readonly AlumniDbContext _context;

        public CommentService(AlumniDbContext context)
        {
            _context = context;
        }
        
        public async Task<Comment> GetByIdAsync(int id)
        {
            return await _context.Comments.FindAsync(id);
        }

        public async Task<IEnumerable<Comment>> GetAllAsync()
        {
            return await _context.Comments.ToListAsync();
        }

        public async Task<Comment> AddAsync(Comment comment)
        {
            _context.Comments.Add(comment);
            await _context.SaveChangesAsync();
            return comment;
        }
        public async Task UpdateAsync(Comment comment)
        {
            _context.Entry(comment).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var comment = await _context.Comments.FindAsync(id);
            _context.Comments.Remove(comment);
            await _context.SaveChangesAsync();
        }

        public bool EntityExists(int id)
        {
            return _context.Comments.Any(e => e.CommentId == id);
        }

        public async Task EditCommentAsync(int id, string content)
        {
            Comment commentToUpdate = await _context.Comments
                .FindAsync(id);

            commentToUpdate.Content = content;
            await _context.SaveChangesAsync();
        }
    }
}
