﻿using Alumni_backend.DAL;
using Alumni_backend.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Alumni_backend.Service;
using Alumni_backend.Models.DTOs.User;

namespace Alumni_backend.Service
{
    public class UserService : IUserService
    {
        public readonly AlumniDbContext _context;

        public UserService(AlumniDbContext context)
        {
            _context = context;
        }

        public async Task<User> AddAsync(User entity)
        {
            _context.Users.Add(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task DeleteAsync(int id)
        {
            var movie = await _context.Users.FindAsync(id);
            _context.Users.Remove(movie);
            await _context.SaveChangesAsync();

        }

        public bool EntityExists(int id)
        {
            return _context.Users.Any(e => e.UserId == id);
        }

        public bool EntityExists(string email)
        {
            return _context.Users.Any(e => e.Email == email);
        }

        public async Task<IEnumerable<User>> GetAllAsync()
        {

            return await _context.Users
                .Include(u => u.AuthoredPosts)
                .Include(u => u.AuthoredComments)
                .Include(u => u.SubscribedPosts)
                .Include(u => u.Topics)
                .Include(u => u.Groups)
                .ToListAsync();

        }

        public async Task<User> GetByIdAsync(int id)
        {
            return await _context.Users
                .Include(u => u.AuthoredPosts)
                .Include(u => u.AuthoredComments)
                .Include(u => u.SubscribedPosts)
                .Include(u => u.Topics)
                .Include(u => u.Groups)
                .Where(u => u.UserId == id)
                .FirstAsync();
        }

        public async Task<IEnumerable<User>> GetMultipleUsers(List<int> users)
        {
            var foundUsers =await  _context.Users.Where(user => users.Contains(user.UserId)).ToListAsync();
            return foundUsers;
        }

        public async Task<User> GetByEmailAsync(string email)
        {
            var user = await _context.Users
                .Include(u => u.AuthoredPosts)
                .Include(u => u.AuthoredComments)
                .Include(u => u.SubscribedPosts)
                .Include(u => u.Topics)
                .Include(u => u.Groups)
                .Where(u => u.Email == email)
                .FirstAsync();
            return user;
        }

        public async Task UpdateAsync(int id, UserEditDTO dto)
        {
            User userToUpdate = await _context.Users.FindAsync(id);

            userToUpdate.Name = dto.Name;
            userToUpdate.ImgUrl = dto.ImgURL;
            userToUpdate.Email = dto.Email;
            userToUpdate.Occupation = dto.Occupation;
            userToUpdate.Bio = dto.Bio;
            userToUpdate.FunFact = dto.FunFact;
            userToUpdate.Provider = dto.Provider;
            userToUpdate.Name = dto.Name;

            await _context.SaveChangesAsync();

        }
        /*

        public async Task UpdateUserAsync(int id, UserEditDTO userDto)
        {
            User userToUpdate = await _context.Users.FindAsync(id);
            userToUpdate.
        }
        */

        public Task<ICollection<Post>> GetPostsForUser(int id)
        {
            throw new NotImplementedException();
        }
    }
}
