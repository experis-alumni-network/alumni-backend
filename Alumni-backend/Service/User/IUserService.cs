﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Alumni_backend.Models.Domain;
using Alumni_backend.Models.DTOs.User;

namespace Alumni_backend.Service
{
    public interface IUserService
    {
        //This interface wil containt all methods that are specific for only the USER model
        public Task<ICollection<Post>> GetPostsForUser(int id);
        public Task<IEnumerable<User>> GetAllAsync();
        public Task<User> GetByIdAsync(int Id);
        public Task<User> AddAsync(User entity);
        public Task DeleteAsync(int id);
        public Task UpdateAsync(int id, UserEditDTO dto);
        public bool EntityExists(int id);
    }
}
