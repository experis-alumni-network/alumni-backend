﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alumni_backend.Service
{
    /// <summary>
    /// Base interface for CRUD functionality, will be implemented by most servicses.
    /// </summary>
    /// <typeparam name="T">
    /// Value so each implementation of this interface can accept different models.
    /// </typeparam>
    public interface IService<T>
    {
        public Task<IEnumerable<T>> GetAllAsync();
        public Task<T> GetByIdAsync(int Id);
        public Task<T> AddAsync(T entity);
        public Task DeleteAsync(int id);
        public bool EntityExists(int id);
    }
}
