﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Alumni_backend.Migrations
{
    public partial class a : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Topics",
                columns: table => new
                {
                    TopicId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Topics", x => x.TopicId);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    UserId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Email = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: false),
                    Name = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: false),
                    ImgUrl = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Occupation = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: false),
                    Bio = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: false),
                    FunFact = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: false),
                    Provider = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "Groups",
                columns: table => new
                {
                    GroupId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    IsPrivate = table.Column<bool>(type: "bit", nullable: false),
                    AdminId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Groups", x => x.GroupId);
                    table.ForeignKey(
                        name: "FK_Groups_Users_AdminId",
                        column: x => x.AdminId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TopicUser",
                columns: table => new
                {
                    InterestedUsersUserId = table.Column<int>(type: "int", nullable: false),
                    TopicsTopicId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TopicUser", x => new { x.InterestedUsersUserId, x.TopicsTopicId });
                    table.ForeignKey(
                        name: "FK_TopicUser_Topics_TopicsTopicId",
                        column: x => x.TopicsTopicId,
                        principalTable: "Topics",
                        principalColumn: "TopicId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TopicUser_Users_InterestedUsersUserId",
                        column: x => x.InterestedUsersUserId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GroupUser",
                columns: table => new
                {
                    UserId = table.Column<int>(type: "int", nullable: false),
                    GroupId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupUser", x => new { x.UserId, x.GroupId });
                    table.ForeignKey(
                        name: "FK_GroupUser_Groups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "Groups",
                        principalColumn: "GroupId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GroupUser_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Posts",
                columns: table => new
                {
                    PostId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Content = table.Column<string>(type: "nvarchar(600)", maxLength: 600, nullable: false),
                    LastUpdated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    AuthorUserId = table.Column<int>(type: "int", nullable: false),
                    Type = table.Column<int>(type: "int", nullable: false),
                    GroupId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Posts", x => x.PostId);
                    table.ForeignKey(
                        name: "FK_Posts_Groups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "Groups",
                        principalColumn: "GroupId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Posts_Users_AuthorUserId",
                        column: x => x.AuthorUserId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    CommentId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Content = table.Column<string>(type: "nvarchar(400)", maxLength: 400, nullable: false),
                    PostId = table.Column<int>(type: "int", nullable: false),
                    AuthorId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.CommentId);
                    table.ForeignKey(
                        name: "FK_Comments_Posts_PostId",
                        column: x => x.PostId,
                        principalTable: "Posts",
                        principalColumn: "PostId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Comments_Users_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PostEvents",
                columns: table => new
                {
                    PostEventId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AllowedGuests = table.Column<int>(type: "int", nullable: false),
                    StartTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    EndTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    BannerImg = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsPrivate = table.Column<bool>(type: "bit", nullable: false),
                    RelatedPostId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostEvents", x => x.PostEventId);
                    table.ForeignKey(
                        name: "FK_PostEvents_Posts_RelatedPostId",
                        column: x => x.RelatedPostId,
                        principalTable: "Posts",
                        principalColumn: "PostId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PostTopic",
                columns: table => new
                {
                    PostId = table.Column<int>(type: "int", nullable: false),
                    TopicId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostTopic", x => new { x.PostId, x.TopicId });
                    table.ForeignKey(
                        name: "FK_PostTopic_Posts_PostId",
                        column: x => x.PostId,
                        principalTable: "Posts",
                        principalColumn: "PostId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PostTopic_Topics_TopicId",
                        column: x => x.TopicId,
                        principalTable: "Topics",
                        principalColumn: "TopicId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PostUser",
                columns: table => new
                {
                    SubscribedPostsPostId = table.Column<int>(type: "int", nullable: false),
                    SubscribedUsersUserId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostUser", x => new { x.SubscribedPostsPostId, x.SubscribedUsersUserId });
                    table.ForeignKey(
                        name: "FK_PostUser_Posts_SubscribedPostsPostId",
                        column: x => x.SubscribedPostsPostId,
                        principalTable: "Posts",
                        principalColumn: "PostId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PostUser_Users_SubscribedUsersUserId",
                        column: x => x.SubscribedUsersUserId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Invitations",
                columns: table => new
                {
                    InvitationId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Answer = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    EventId = table.Column<int>(type: "int", nullable: false),
                    GuestId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Invitations", x => x.InvitationId);
                    table.ForeignKey(
                        name: "FK_Invitations_PostEvents_EventId",
                        column: x => x.EventId,
                        principalTable: "PostEvents",
                        principalColumn: "PostEventId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Invitations_Users_GuestId",
                        column: x => x.GuestId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Topics",
                columns: new[] { "TopicId", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "The future", "AI" },
                    { 2, ".NET stuff", ".NET" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "UserId", "Bio", "Email", "FunFact", "ImgUrl", "Name", "Occupation", "Provider" },
                values: new object[,]
                {
                    { 1, "This guy learning git is literally equivalent to a hippo trying to learn how to fly", "henrik@gmail.com", "Tall boi", "https://g.acdn.no/obscura/API/dynamic/r1/ece5/tr_1000_2000_s_f/0000/osbl/2016/9/26/11/1474881616150.jpg?chk=B9ADD5", "Henrik Eijsink", "student", null },
                    { 2, "Defeats dark lords for a living", "theChosenOne@hogwarts.uk", "Scar in my face was actually just me tripping and faceplanting in 3rd grade", "https://www.temashop.no/media/catalog/product/cache/cat_resized/1200/0/h/a/harry_potter_briller_tilbeh_r_briller.jpg", "Harry Potter", "student", null },
                    { 3, "Books. I read them all", "books4ever@hogwarts.uk", "I was always in love with Harry", "https://no.erch2014.com/images/iskusstvo-i-razvlecheniya/kakoe-zhe-ee-nastoyashee-imya-germiona-grejndzher.jpg", "Hermoine Granger", "student", null },
                    { 4, "I love spiders", "ronnyw@hogwarts.uk", "Can't properly pronounce Wingardium Leviosa", "https://static.wikia.nocookie.net/hpmor/images/8/83/Ron_Weasley.jpeg/revision/latest/top-crop/width/360/height/450?cb=20190217164742", "Ron Weasley", "student", null },
                    { 5, "Running out of creativity", "ablus@hogwarts.uk", "Has a burning bird", "https://www.looper.com/img/gallery/dumbledores-backstory-explained/intro-1567606121.jpg", "Albus Dumbledore", "Headmaster", null },
                    { 6, "I am the best", "harrypottersucks@hogwarts.uk", "I love my daddy", "https://pbs.twimg.com/profile_images/1298380607068745729/t65sdSut.jpg", "Draco Malfoy", "student", null }
                });

            migrationBuilder.InsertData(
                table: "Groups",
                columns: new[] { "GroupId", "AdminId", "Description", "IsPrivate", "Name" },
                values: new object[,]
                {
                    { 1, 1, "Group for noroff 2021", false, "Noroff Summer 2021" },
                    { 2, 2, "Developers for alumni stuff", false, "Alumni-Dev" }
                });

            migrationBuilder.InsertData(
                table: "Posts",
                columns: new[] { "PostId", "AuthorUserId", "Content", "CreationDate", "GroupId", "LastUpdated", "Title", "Type" },
                values: new object[,]
                {
                    { 3, 1, "We should just use Ai, am i right", new DateTime(2015, 6, 15, 13, 45, 0, 0, DateTimeKind.Unspecified), null, new DateTime(2021, 6, 15, 13, 45, 0, 0, DateTimeKind.Unspecified), "Why Ai is the future of teaching", 0 },
                    { 50, 1, "Come to my Hell's Kitchen watch party you idiot sandwiches!", new DateTime(2015, 6, 15, 13, 45, 0, 0, DateTimeKind.Unspecified), null, new DateTime(2021, 6, 15, 13, 45, 0, 0, DateTimeKind.Unspecified), "Hell's Kitchen watch party", 1 },
                    { 4, 6, "I believe .net is bether then java, because i like it bether!!", new DateTime(2021, 10, 21, 8, 10, 0, 0, DateTimeKind.Unspecified), null, new DateTime(2021, 10, 21, 8, 0, 0, 0, DateTimeKind.Unspecified), "Why i believe .net is bether then Java", 0 },
                    { 5, 6, "This is a gathering for us developers that love .net, not JAVA!!", new DateTime(2021, 10, 21, 9, 10, 0, 0, DateTimeKind.Unspecified), null, new DateTime(2021, 10, 21, 9, 0, 0, 0, DateTimeKind.Unspecified), "Gathering of .net developers. NOT JAVA", 1 }
                });

            migrationBuilder.InsertData(
                table: "GroupUser",
                columns: new[] { "GroupId", "UserId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 2 }
                });

            migrationBuilder.InsertData(
                table: "PostEvents",
                columns: new[] { "PostEventId", "AllowedGuests", "BannerImg", "EndTime", "IsPrivate", "RelatedPostId", "StartTime" },
                values: new object[,]
                {
                    { 4, 200, "https://i.pinimg.com/originals/bc/34/61/bc3461ec3ed85a9822ee90be77ff48fc.jpg", new DateTime(2021, 10, 28, 22, 2, 19, 273, DateTimeKind.Local).AddTicks(6682), false, 50, new DateTime(2021, 10, 28, 22, 2, 19, 273, DateTimeKind.Local).AddTicks(6678) },
                    { 3, 200, "https://upload.wikimedia.org/wikipedia/commons/a/a3/.NET_Logo.svg", new DateTime(2021, 10, 28, 22, 2, 19, 273, DateTimeKind.Local).AddTicks(6674), false, 5, new DateTime(2021, 10, 28, 22, 2, 19, 273, DateTimeKind.Local).AddTicks(6658) }
                });

            migrationBuilder.InsertData(
                table: "PostTopic",
                columns: new[] { "PostId", "TopicId" },
                values: new object[,]
                {
                    { 3, 1 },
                    { 4, 2 },
                    { 5, 2 }
                });

            migrationBuilder.InsertData(
                table: "Posts",
                columns: new[] { "PostId", "AuthorUserId", "Content", "CreationDate", "GroupId", "LastUpdated", "Title", "Type" },
                values: new object[,]
                {
                    { 1, 1, "Sometimes i strugle with Git, i dont know why. Any help?", new DateTime(2015, 6, 15, 13, 45, 0, 0, DateTimeKind.Unspecified), 1, new DateTime(2021, 6, 15, 13, 45, 0, 0, DateTimeKind.Unspecified), "Why is git so weird to use, sometimes?", 0 },
                    { 2, 3, "Welcome to a day where we are learning to use Ai, to reduce the amount of code we need to wright", new DateTime(2015, 6, 15, 13, 45, 0, 0, DateTimeKind.Unspecified), 1, new DateTime(2021, 6, 15, 13, 45, 0, 0, DateTimeKind.Unspecified), "Learn to use Ai to autocomplete code.", 1 }
                });

            migrationBuilder.InsertData(
                table: "Comments",
                columns: new[] { "CommentId", "AuthorId", "Content", "PostId" },
                values: new object[] { 1, 3, "AGREED", 1 });

            migrationBuilder.InsertData(
                table: "PostEvents",
                columns: new[] { "PostEventId", "AllowedGuests", "BannerImg", "EndTime", "IsPrivate", "RelatedPostId", "StartTime" },
                values: new object[] { 1, 25, "https://www.sciencenewsforstudents.org/wp-content/uploads/2021/08/LL_AI_feat-1030x580.jpg", new DateTime(2021, 10, 28, 22, 2, 19, 273, DateTimeKind.Local).AddTicks(5098), false, 2, new DateTime(2021, 10, 28, 22, 2, 19, 270, DateTimeKind.Local).AddTicks(9194) });

            migrationBuilder.InsertData(
                table: "PostTopic",
                columns: new[] { "PostId", "TopicId" },
                values: new object[] { 2, 1 });

            migrationBuilder.InsertData(
                table: "Invitations",
                columns: new[] { "InvitationId", "Answer", "EventId", "GuestId" },
                values: new object[] { 1, "YES", 1, 1 });

            migrationBuilder.InsertData(
                table: "Invitations",
                columns: new[] { "InvitationId", "Answer", "EventId", "GuestId" },
                values: new object[] { 2, "NO", 1, 2 });

            migrationBuilder.CreateIndex(
                name: "IX_Comments_AuthorId",
                table: "Comments",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_PostId",
                table: "Comments",
                column: "PostId");

            migrationBuilder.CreateIndex(
                name: "IX_Groups_AdminId",
                table: "Groups",
                column: "AdminId");

            migrationBuilder.CreateIndex(
                name: "IX_GroupUser_GroupId",
                table: "GroupUser",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_Invitations_EventId",
                table: "Invitations",
                column: "EventId");

            migrationBuilder.CreateIndex(
                name: "IX_Invitations_GuestId",
                table: "Invitations",
                column: "GuestId");

            migrationBuilder.CreateIndex(
                name: "IX_PostEvents_RelatedPostId",
                table: "PostEvents",
                column: "RelatedPostId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_AuthorUserId",
                table: "Posts",
                column: "AuthorUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_GroupId",
                table: "Posts",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_PostTopic_TopicId",
                table: "PostTopic",
                column: "TopicId");

            migrationBuilder.CreateIndex(
                name: "IX_PostUser_SubscribedUsersUserId",
                table: "PostUser",
                column: "SubscribedUsersUserId");

            migrationBuilder.CreateIndex(
                name: "IX_TopicUser_TopicsTopicId",
                table: "TopicUser",
                column: "TopicsTopicId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: "GroupUser");

            migrationBuilder.DropTable(
                name: "Invitations");

            migrationBuilder.DropTable(
                name: "PostTopic");

            migrationBuilder.DropTable(
                name: "PostUser");

            migrationBuilder.DropTable(
                name: "TopicUser");

            migrationBuilder.DropTable(
                name: "PostEvents");

            migrationBuilder.DropTable(
                name: "Topics");

            migrationBuilder.DropTable(
                name: "Posts");

            migrationBuilder.DropTable(
                name: "Groups");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
