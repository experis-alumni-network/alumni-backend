# Alumni-Backend
Made by Henrik Eijsink, Martin Johansen, Martin Stenberg, Tien Nguyen and Sinan Karagülle

This backend is hosted on azure.

To run this backend locally you need:

- MSSQL server
- Visual Studio (To set up database, using Entity Framework, and to run the code)
- Change server url to your locall connection string in, startup and DBcontext. 
